# uROS firmware

uROS stands for micro Read-Out Server. It is the production firmware for the second-level electronics system of the CMS DT readout chain. It receives data from the ROB board and delivers it to the AMC13, which in term delivers it to the DAQ.

It runs in the TM7 board hw.

## Structure of the project

### /uROS

This folder contains the vivado project itself. The only versioned files here whould be the vivado project file top.xpr and the final generated bitfile (top.bit).

There should be no source files in this folder. However, for historical reasons, there are some IP cores in the /vivado/top/top.srcs/sources_1/ip folder.

## /cactusupgrades

This is the main sources folder. 

The first firmware developed for TM7 board was for the TwinMux role (replacement of the TSC in the phase-1 upgrade). Because it needed to send data to the track finders, that were running mp7 boards, in a specific protocol, the first project was not created from scratch, but instead by modifying an mp7 board. Some fw modules that were imported directly from the mp7, and we kept also part of the MP7 SW that was used to communicate with those modules. Because of this, and in order to make it easier to backport changes from the mp7 development to the TM7-based firmwares, we have decided to keep the general folder structure.

This folder has an structure that is equivalent to (and partly copied from) the cactusupgrades folder at https://gitlab.cern.ch/cms-cactus/firmware/mp7

The ipbus firmware was originally (in the SVN) hosted together with the rest of the mp7 packages, but later on, it began being used outside of cms, and even outside of cern, and because of that, it eventually started to be hosted at https://github.com/ipbus/

The backport of changes from their original sources at the mp7 and ipbus repos is not automatic. The details of the relationship of files used in this project to their original sources can be found on folder /aux_data/cactusdiffs.md

### aux_data

Other auxiliary data.

