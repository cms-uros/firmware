----------------------------------------------------------------------------------
-- �lvaro Navarro, CIEMAT, 2017
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--Library UNIMACRO;
--use UNIMACRO.vcomponents.all;

library work;
use work.uros_readout_decl.all;

entity muxfreezecount is
    Generic ( 
        SIZE        : natural := 32; 
        N_CTRS      : natural := 32; 
        MUX_SIZE    : natural := 5;
        USE_DSP     : string  := "no"
        );
    port (
        ctrs_inc    : in std_logic_vector(N_CTRS-1 downto 0);
        ctr_ena     : in std_logic;
        freeze      : in std_logic;
        mux         : in unsigned(MUX_SIZE-1 downto 0);
        rst         : in std_logic;
        clk         : in std_logic;
        output      : out unsigned(SIZE - 1 downto 0)
       );
       
end muxfreezecount;

architecture Behavioral of muxfreezecount is

    type counter_v_t is array(N_CTRS-1 downto 0) of unsigned(SIZE - 1 downto 0);
    signal counter_v, counter_v_latched : counter_v_t;
    attribute use_dsp48 : string;
    attribute use_dsp48 of counter_v : signal is USE_DSP;

begin

process(clk)
begin
    if rising_edge(clk) then
        if rst = '1' then
            counter_v           <= ( others => (others => '0') );
        else        
            for ch in N_CTRS - 1 downto 0 loop
                counter_v(ch) <= counter_v(ch) + bo2int(ctrs_inc(ch) = '1' and ctr_ena = '1');
            end loop;
        end if;
    end if;
end process;

counter_v_latched <= counter_v when freeze = '0' else counter_v_latched;
output  <= counter_v_latched(to_integer(mux));

end Behavioral;
