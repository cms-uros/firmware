----------------------------------------------------------------------------------
-- Readout data types
-- �lvaro Navarro, CIEMAT, 2015
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

package uros_readout_decl is

    constant RO_BXN_WIDTH : integer := 12;
    constant RO_EVN_WIDTH : integer := 24;
    constant RO_ORN_WIDTH : integer := 16;

    constant ROBSTAT_COUNTERS : integer := 22;

    constant STATREG_ROBSTAT_FIRST : integer := 34;
    
    constant ROBFIFO_DEPTH : integer := 11;

    type slv12_v_t is array(natural range <>) of std_logic_vector(11 downto 0);
    type slv91_v_t is array(natural range <>) of std_logic_vector(90 downto 0);
    type slv32_v_t is array(natural range <>) of std_logic_vector(31 downto 0);
    type uint8_v_t is array(natural range <>) of unsigned(7 downto 0); 
    type uintFFD_v_t is array(natural range <>) of unsigned(ROBFIFO_DEPTH-1 downto 0); 

    type rrstats_t is record
        netedgeshift : unsigned(7 downto 0);
        
        w12_data     : std_logic_vector(11 downto 0);
        w12_weak     : std_logic_vector(11 downto 0);
        w12_samples  : std_logic_vector(90 downto 0);
        w12_toggle   : std_logic;
        w12_lock     : std_logic;
        
        ff_max_occupancy : unsigned(ROBFIFO_DEPTH-1 downto 0);
        
        w12_data_snapshot       : std_logic_vector(11 downto 0);
        w12_weak_snapshot       : std_logic_vector(11 downto 0);
        w12_samples_snapshot    : std_logic_vector(90 downto 0);
        
        -- one-cicle signals for counter increase
        counters_inc    : std_logic_vector(ROBSTAT_COUNTERS - 1 downto 0);
    end record;

    type rrstats_v_t is array(natural range <>) of rrstats_t;

    constant rrstats_init : rrstats_t := (
        netedgeshift => (others => '0'),
        
        w12_data     => (others => '0'),
        w12_weak     => (others => '0'),
        w12_samples  => (others => '0'),
        w12_toggle   => '0',
        w12_lock     => '0',
        
        w12_data_snapshot       => (others => '0'),
        w12_weak_snapshot       => (others => '0'),
        w12_samples_snapshot    => (others => '0'),
        
        ff_max_occupancy    => (others => '0'),
        
        -- one-cicle signals for counter increase
        counters_inc    => (others => '0')
        );

    type bo2sl_t is array (boolean) of std_logic;
    constant bo2sl : bo2sl_t := (false => '0', true => '1');

    type bo2int_t is array (boolean) of integer range 1 downto 0;
    constant bo2int : bo2int_t := (false => 0, true => 1);

end uros_readout_decl;

package body uros_readout_decl is

end uros_readout_decl;
