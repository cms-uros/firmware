----------------------------------------------------------------------------------
-- Wrapper for Boston University's DAQ_Link_7S module.
-- Basically instantiates it together with the MGT core and some logic that was missing after removal of
-- the core from the DL module.
-- �lvaro Navarro, CIEMAT, 2015
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_misc.all; --used for the or_reduce in code copied from BU's release-1.2

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

library work;


entity tm7_amc13link is
    Port ( 
        sysclk                  : in std_logic;
        reset                   : in STD_LOGIC; -- asynchronous reset, assert reset until GTX REFCLK stable
        -- MGT ports
        AMC_P1_RX_P             : in std_logic;
        AMC_P1_RX_N             : in std_logic;
        AMC_P1_TX_N             : out std_logic;
        AMC_P1_TX_P             : out std_logic;
        amc13_refclk            : in STD_LOGIC;
        --MGT_CLK_115_1_P         : in STD_LOGIC;
        --MGT_CLK_115_1_N         : in STD_LOGIC;
        -- TTS port
        TTSclk                  : in STD_LOGIC; -- clock source which clocks TTS signals
        TTS                     : in STD_LOGIC_VECTOR (3 downto 0);
        -- Data port
        ReSyncAndEmpty          : in STD_LOGIC;
        EventDataClk            : in STD_LOGIC;
        EventData_valid         : in STD_LOGIC; -- used as data write enable
        EventData_header        : in STD_LOGIC; -- first data word
        EventData_trailer       : in STD_LOGIC; -- last data word
        EventData               : in STD_LOGIC_VECTOR (63 downto 0);
        AlmostFull              : out STD_LOGIC; -- buffer almost full
        Ready                   : out STD_LOGIC;
        -- Debug port
        debug                   : out std_logic_vector(31 downto 0)
        );
end tm7_amc13link;

architecture Behavioral of tm7_amc13link is

    signal usrclk ,txoutclk                                     : std_logic;
--    signal mgtclk115_1                                          : std_logic;
    signal cplllock, rxresetdone, txfsmresetdone, data_valid    : std_logic;
    signal rxnotintable, rxchariscomma, rxcharisk, txcharisk    : std_logic_vector(1 downto 0);
    signal rxdata, txdata                                       : std_logic_vector(15 downto 0);
--    signal rxresetdoneSyncRegs    : std_logic_vector(2 downto 0)  := (others => '0');
--    signal K_Cntr              : unsigned(7 downto 0)  := (others => '0');

    signal rxrstdbg, txrstdbg, data_valid_d : std_logic;
    signal errctr                           : unsigned(15 downto 0);


begin

    i_usrclk : BUFH 
    port map (O => usrclk, I => txoutclk);

    --     MGT Clock Instance
--   IBUFDS_GTE2_inst : IBUFDS_GTE2
--   port map (
--      O => mgtclk115_1,         -- 1-bit output: Refer to Transceiver User Guide
--      ODIV2 => open, -- 1-bit output: Refer to Transceiver User Guide
--      CEB => '0',     -- 1-bit input: Refer to Transceiver User Guide
--      I => MGT_CLK_115_1_P,         -- 1-bit input: Refer to Transceiver User Guide
--      IB => MGT_CLK_115_1_N        -- 1-bit input: Refer to Transceiver User Guide
--   );


    -------------------------------------------------------------------------------
    -------------------------------------------------------------------------------
    -- BOSTON UNIVERSITY's entity
    -------------------------------------------------------------------------------
    -------------------------------------------------------------------------------
    
    i_DAQ_Link_7S: entity work.DAQ_Link_7S
    generic map(
        -- If you do not use the trigger port, set it to false
        simulation          => False)
    port map(
        reset               => reset,  -- asynchronous reset, assert reset until GTX REFCLK stable
        USE_TRIGGER_PORT    => False,
        -- MGT signals
        usrclk              => usrclk            ,
        cplllock            => cplllock            ,
        rxresetdone         => rxresetdone        ,
        txfsmresetdone      => txfsmresetdone    ,
        rxnotintable        => rxnotintable    ,
        rxchariscomma       => rxchariscomma    ,
        rxcharisk           => rxcharisk        ,
        rxdata              => rxdata            ,
        txcharisk           => txcharisk        ,
        txdata              => txdata            ,
        -- TRIGGER port
        TTCclk              => '0'                ,
        BcntRes             => '0'                ,
        trig                => "00000000"        ,
        -- TTS port
        TTSclk              => TTSclk                ,  -- clock source which clocks TTS signals
        TTS                 => TTS                    ,
        -- Data port
        ReSyncAndEmpty      => ReSyncAndEmpty   ,
        EventDataClk        => EventDataClk      ,
        EventData_valid     => EventData_valid   , -- used as data write enable
        EventData_header    => EventData_header  , -- first data word
        EventData_trailer   => EventData_trailer , -- last data word
        EventData           => EventData         ,
        AlmostFull          => AlmostFull        , -- buffer almost full
        Ready               => Ready                    ,
        sysclk              => sysclk            ,
        L1A_DATA_we         => open                    ,
        L1A_DATA            => open                    
        );

    -------------------------------------------------------------------------------
    -------------------------------------------------------------------------------
    -- GTH Core
    -------------------------------------------------------------------------------
    -------------------------------------------------------------------------------

    amc13_gth : entity work.amc13_link_gt
    port map(
        SYSCLK_IN                       =>      sysclk,
        SOFT_RESET_TX_IN                =>      '0',
        SOFT_RESET_RX_IN                =>      '0',
        DONT_RESET_ON_DATA_ERROR_IN     =>      '0',
        GT0_TX_FSM_RESET_DONE_OUT => txfsmresetdone,
        GT0_RX_FSM_RESET_DONE_OUT => rxrstdbg,
        GT0_data_valid_IN => data_valid,
    --_________________________________________________________________________
    --GT0  (X0Y9)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt0_cpllfbclklost_out           =>      OPEN,
        gt0_cplllock_out                =>      cplllock,
        gt0_cplllockdetclk_in           =>      sysclk,
        gt0_cpllreset_in                =>      reset,
    -------------------------- Channel - Clocking Ports ------------------------
        gt0_gtrefclk0_in                =>      '0',
        gt0_gtrefclk1_in                =>      amc13_refclk,
    ---------------------------- Channel - DRP Ports  --------------------------
        gt0_drpclk_in                   =>      sysclk,
    --------------------- RX Initialization and Reset Ports --------------------
        gt0_eyescanreset_in             =>      '0', -- Improvisando
        gt0_rxuserrdy_in                =>      '0',
    -------------------------- RX Margin Analysis Ports ------------------------
        gt0_eyescandataerror_out        =>      open,
        gt0_eyescantrigger_in           =>      '0', -- Improvisando
    ------------------- Receive Ports - Clock Correction Ports -----------------
        gt0_rxclkcorcnt_out             =>      open, -- Improvisando
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt0_dmonitorout_out             =>      open, -- Improvisando
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt0_rxusrclk_in                 =>      usrclk,
        gt0_rxusrclk2_in                =>      usrclk,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt0_rxdata_out                  =>      rxdata,
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt0_rxdisperr_out               =>      open,
        gt0_rxnotintable_out            =>      rxnotintable,
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt0_gthrxn_in                   =>      AMC_P1_RX_N,
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt0_rxmonitorout_out            =>      open, -- Improvisando
        gt0_rxmonitorsel_in             =>      (others => '0'), -- Improvisando
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt0_gtrxreset_in                =>      reset,
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt0_rxchariscomma_out           =>      rxchariscomma,
        gt0_rxcharisk_out               =>      rxcharisk,
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt0_gthrxp_in                   =>      AMC_P1_RX_P,
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt0_rxresetdone_out             =>      rxresetdone,
    --------------------- TX Initialization and Reset Ports --------------------
        gt0_gttxreset_in                =>      reset,
        gt0_txuserrdy_in                =>      '0',
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        gt0_txusrclk_in                 =>      usrclk,
        gt0_txusrclk2_in                =>      usrclk,
    ------------------ Transmit Ports - TX Data Path interface -----------------
        gt0_txdata_in                   =>      txdata,
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt0_gthtxn_out                  =>      AMC_P1_TX_N,
        gt0_gthtxp_out                  =>      AMC_P1_TX_P,
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt0_txoutclk_out                =>      txoutclk,
        gt0_txoutclkfabric_out          =>      open,
        gt0_txoutclkpcs_out             =>      open,
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt0_txresetdone_out             =>      txrstdbg,
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt0_txcharisk_in                =>      txcharisk,


    --____________________________COMMON PORTS________________________________
     GT0_QPLLOUTCLK_IN  => '0',
     GT0_QPLLOUTREFCLK_IN => '0' 

);


--    process(usrclk)
--    begin
--        if rising_edge(usrclk) then
--            if(rxcharisk = "11" and rxdata = x"3cbc")then
--                data_valid <= '1';
--            elsif(rxresetdoneSyncRegs(2) = '0' or or_reduce(rxnotintable) = '1')then
--                data_valid <= '0';
--            end if;
--        end if;
--    end process;

--    process(usrclk, rxresetdone)
--    begin
--        if(rxresetdone = '0')then
--            rxresetdoneSyncRegs <= (others => '0');
--        elsif rising_edge(usrclk) then
--            rxresetdoneSyncRegs <= rxresetdoneSyncRegs(1 downto 0) & '1';
--        end if;
--    end process;
    
    process(usrclk)
    begin
        if rising_edge(usrclk) then
            if rxresetdone = '0' or txfsmresetdone = '0' or or_reduce(rxnotintable) = '1' or cplllock = '0' then
                data_valid <= '0';
            elsif rxcharisk = "11" and rxdata = X"3cbc" then
                data_valid <= '1';
            end if;
        end if;
    end process;

    process(sysclk)
    begin
        if rising_edge(sysclk) then
            if reset = '1' then
                errctr <= (others => '0');
            elsif data_valid <= '0' and data_valid_d <= '1' and errctr /= X"ffff" then
                errctr <= errctr + 1;
            end if;
            data_valid_d <= data_valid;
        end if;
    end process;


    -- Debug

    debug(0) <= rxresetdone;
    debug(1) <= txfsmresetdone;
    debug(2) <= data_valid;
    debug(3) <= rxrstdbg;
    debug(4) <= cplllock;
    debug(5) <= txrstdbg;
    debug(6) <= '0';
    debug(7) <= '0';
    debug(23 downto 8) <= std_logic_vector(errctr);


end Behavioral;

