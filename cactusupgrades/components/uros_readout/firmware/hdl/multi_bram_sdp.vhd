----------------------------------------------------------------------------------
-- Multi-BRAM_SDP_MACRO memory.
-- The model for distribution of data is equivalent to the "minimum power" option in the Wizard
-- �lvaro Navarro, CIEMAT, 2015
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

Library UNIMACRO;
use UNIMACRO.vcomponents.all;

library work;
use work.minmax.all;

entity multi_bram_sdp is
	Generic (
		DATA_WIDTH        : natural := 8;
		DEPTH_LOG2        : natural := 11;
		BR18_ADDR_WIDTH   : natural := 9
		-- Maximum "macro data width" for each MACRO_ADDR_WIDTH:
		-- 			9 --> 36
		-- 			10 --> 18
		-- 			11 --> 9
		-- 			12 --> 4
		-- 			13 --> 2
		-- 			14 --> 1
		);
	Port (
		 din  : IN 	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
		 wadd : IN 	STD_LOGIC_VECTOR(DEPTH_LOG2-1 DOWNTO 0);
		 wen  : IN 	STD_LOGIC;
		 wclk : IN 	STD_LOGIC;
		 
		 dout : OUT STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
		 radd : IN 	STD_LOGIC_VECTOR(DEPTH_LOG2-1 DOWNTO 0);
		 ren  : IN 	STD_LOGIC;
		 rclk : IN 	STD_LOGIC
		);
end multi_bram_sdp;

architecture Behavioral of multi_bram_sdp is

	constant BR18_DATA_WIDTH	: positive := 36 / 2**(BR18_ADDR_WIDTH - 9);
	constant columns 				: positive := 1 + (DATA_WIDTH-1) / BR18_DATA_WIDTH;
	constant rows 					: positive := 2**(maximum(DEPTH_LOG2 -BR18_ADDR_WIDTH,0));
	constant N_BRAM18 			: positive := columns * rows;
	constant we_size				: positive := 2**(maximum(11-BR18_ADDR_WIDTH,0));
	
	type D_t is array (N_BRAM18 - 1 downto 0) of std_logic_vector(BR18_DATA_WIDTH -1 downto 0);
	signal DO, DI : D_t;
	signal RDEN, WREN : std_logic_vector(rows - 1 downto 0);
	signal RDADDR, WRADDR : std_logic_vector(BR18_ADDR_WIDTH - 1 downto 0);

	signal rsel, wsel 	: integer range rows - 1 downto 0 := 0;
	signal dinx, doutx 	: std_logic_vector(columns*BR18_DATA_WIDTH - 1 downto 0) := (others => '0');
	

begin
	
	dinx(din'range) <= din;
	dinx(dinx'high downto din'high+1) <= (others => '0');
	dout <= doutx(dout'range);
	
	RDADDR(minimum(RDADDR'left, radd'left) downto 0) <= radd(minimum(RDADDR'left, radd'left) downto 0);
	WRADDR(minimum(WRADDR'left, wadd'left) downto 0) <= wadd(minimum(WRADDR'left, wadd'left) downto 0);
	
	sel_gen : if rows > 1 generate
		rsel <= to_integer(unsigned(radd(DEPTH_LOG2 - 1 downto BR18_ADDR_WIDTH)));
		wsel <= to_integer(unsigned(wadd(DEPTH_LOG2 - 1 downto BR18_ADDR_WIDTH)));
	end generate;
	sel_gen2 : if rows = 1 generate
		rsel <= 0;
		wsel <= 0;
	end generate;
	
	RDEN <= std_logic_vector(to_unsigned(2**rsel,rows)) when ren = '1' else (others => '0');
	WREN <= std_logic_vector(to_unsigned(2**wsel,rows)) when wen = '1' else (others => '0');
	
	cols_gen : for col in columns - 1 downto 0 generate

		doutx((col+1)*BR18_DATA_WIDTH-1 downto col*BR18_DATA_WIDTH) <= DO(rsel*columns+col);

		rows2_gen : for row in rows - 1 downto 0 generate
			-- this one is left here because it doesnt harm that the Data Input is shared amongst all the BRAMs and it saves logic
			DI(row*columns+col) <= dinx((col+1)*BR18_DATA_WIDTH-1 downto col*BR18_DATA_WIDTH);

			BRAM_SDP_MACRO_inst : BRAM_SDP_MACRO
			generic map (
				BRAM_SIZE => "18Kb", -- Target BRAM, "18Kb" or "36Kb" 
				DEVICE => "7SERIES", -- Target device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
				WRITE_WIDTH => BR18_DATA_WIDTH,    -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
				READ_WIDTH => BR18_DATA_WIDTH,     -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
				DO_REG => 0, -- Optional output register (0 or 1)
				INIT_FILE => "NONE",
				SIM_COLLISION_CHECK => "ALL", -- Collision check enable "ALL", "WARNING_ONLY", 
														-- "GENERATE_X_ONLY" or "NONE"       
				WRITE_MODE => "WRITE_FIRST") -- Specify "READ_FIRST" for same clock or synchronous clocks
													  --  Specify "WRITE_FIRST for asynchrononous clocks on ports
				-- INIT, SRVAL, INIT_xx and INITP_xx declarations not specified ==> all zeroes
			port map (
				DO => DO(row*columns + col),         -- Output read data port, width defined by READ_WIDTH parameter
				DI => DI(row*columns + col),         -- Input write data port, width defined by WRITE_WIDTH parameter
				RDADDR => RDADDR, 	-- Input read address, width defined by read port depth
				RDCLK => rclk,   		-- 1-bit input read clock
				RDEN => RDEN(row),     -- 1-bit input read port enable
				REGCE => '1',   		-- 1-bit input read output register enable
				RST => '0',       	-- 1-bit input reset 
				WE => (we_size-1 downto 0 => '1'),-- Input write enable, width defined by write port depth
				WRADDR => WRADDR, 	-- Input write address, width defined by write port depth
				WRCLK => wclk,   		-- 1-bit input write clock
				WREN => WREN(row));      -- 1-bit input write port enable
		end generate;
	end generate;
end Behavioral;

