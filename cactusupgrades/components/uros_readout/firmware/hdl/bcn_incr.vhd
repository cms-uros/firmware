----------------------------------------------------------------------------------
-- increment the bunch counter
-- �lvaro Navarro, CIEMAT, 2015
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;


library work;
use work.top_decl.all; -- for LHC_BUNCH_COUNT
use work.mp7_ttc_decl.all;


entity bcn_incr is
    port(
        ttc_clk         : in std_logic;
        bunch_ctr       : in bctr_t;
        incr            : in bctr_t;
        bunch_ctr_incr  : out bctr_t;
        orb_ctr_in      : in eoctr_t;
        orb_ctr_out      : out eoctr_t
    );
    
    
end bcn_incr;

architecture Behavioral of bcn_incr is

    signal bunch_ctr_incr_i : bctr_t;
    signal orb_ctr_i : eoctr_t;

begin

    bunch_ctr_incr <= bunch_ctr when incr = x"000" else
                 bunch_ctr_incr_i;

    orb_ctr_out <= orb_ctr_in when incr = x"000" else orb_ctr_i;
    
    process (ttc_clk)
        variable bunch_ctr_tmp : unsigned(12 downto 0);
    begin
        if rising_edge(ttc_clk) then
            -- Add incr to bunch counter
            -- 1 is added to account for the 1-cycle-incr introduced by the assignment inside a sequential process
            bunch_ctr_tmp := unsigned('0' & bunch_ctr) + unsigned('0' & incr) + 1;
            -- By default, orb_ctr will be left as is 
            orb_ctr_i <= orb_ctr_in;
            -- Wrap around LHC_BUNCH_COUNT
            if bunch_ctr_tmp >= LHC_BUNCH_COUNT then
                bunch_ctr_tmp := bunch_ctr_tmp - LHC_BUNCH_COUNT;
                orb_ctr_i <= eoctr_t(unsigned(orb_ctr_in)+1);
            end if; 
            -- Assing signal
            bunch_ctr_incr_i <= std_logic_vector(bunch_ctr_tmp(11 downto 0));
        end if;
    end process;

end Behavioral;
