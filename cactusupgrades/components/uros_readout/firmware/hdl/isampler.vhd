----------------------------------------------------------------------------------
-- Contains per-channel SelectIO resources (IBUFDS, ISERDES)
-- Alvaro Navarro, CIEMAT, 2016
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity isampler is
    generic (
        DESER_FACTOR       : integer := 10);
    port (
        lvds_in_p   : in    std_logic;
        lvds_in_n   : in    std_logic;
        idelaycnt   : in    std_logic_vector(4 downto 0);
        iserdes_q   : out   std_logic_vector(DESER_FACTOR-1 downto 0);
        -- Clock and reset signals
        clk_in      : in    std_logic;                    -- Fast clock from PLL/MMCM 
        clk_div_in  : in    std_logic;                    -- Slow clock from PLL/MMCM
        io_reset    : in    std_logic);                   -- Reset signal for IO circuit
end isampler;

architecture Behavioral of isampler is

    signal data_in : std_logic;
    signal data_delayed : std_logic;
    signal idelaycnt_out : std_logic_vector(4 downto 0);
    signal idelay_ld : std_logic;
    signal clk_in_int_inv : std_logic;
    signal icascade1, icascade2 : std_logic;
    signal iserdes_q_i : std_logic_vector(13 downto 0);

    attribute IODELAY_GROUP : STRING;
    attribute IODELAY_GROUP of idelaye2_inst: label is "lvds_in";
    
begin


ibufds_inst : IBUFDS
    generic map (
        DIFF_TERM  => FALSE, 
        IOSTANDARD => "LVDS"    )
    port map (
        I   => lvds_in_p,
        IB  => lvds_in_n,
        O   => data_in  );

clk_in_int_inv <= not clk_in;
iserdes_q <= iserdes_q_i(DESER_FACTOR-1 downto 0);


idelaye2_inst : IDELAYE2
    generic map (
       CINVCTRL_SEL => "FALSE",          -- Enable dynamic clock inversion (FALSE, TRUE)
       DELAY_SRC => "IDATAIN",           -- Delay input (IDATAIN, DATAIN)
       HIGH_PERFORMANCE_MODE => "FALSE", -- Reduced jitter ("TRUE"), Reduced power ("FALSE")
       IDELAY_TYPE => "VAR_LOAD",        -- FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
       REFCLK_FREQUENCY => 200.0,        -- IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
       SIGNAL_PATTERN => "DATA"          -- DATA, CLOCK input signal
    )
    port map (
       CNTVALUEOUT => idelaycnt_out, -- 5-bit output: Counter value output
       DATAOUT => data_delayed,         -- 1-bit output: Delayed data output
       C => clk_div_in,                     -- 1-bit input: Clock input
       CE => '0',                   -- 1-bit input: Active high enable increment/decrement input
       CINVCTRL => '0',       -- 1-bit input: Dynamic clock inversion input
       CNTVALUEIN => idelaycnt,   -- 5-bit input: Counter value input
       DATAIN => '0',           -- 1-bit input: Internal delay data input
       IDATAIN => data_in,         -- 1-bit input: Data input from the I/O
       INC => '0',                 -- 1-bit input: Increment / Decrement tap delay input
       LD => idelay_ld,                   -- 1-bit input: Load IDELAY_VALUE input
       LDPIPEEN => '0',       -- 1-bit input: Enable PIPELINE register to load data input
       REGRST => '0'            -- 1-bit input: Active-high reset tap-delay input
    );

process(clk_div_in)
begin
    if rising_edge(clk_div_in) then
        if idelaycnt /= idelaycnt_out then
            idelay_ld <= '1';
        else
            idelay_ld <= '0';
        end if;
    end if;
end process;


iserdese2_master : ISERDESE2
    generic map (
        DATA_RATE         => "DDR",
        DATA_WIDTH        => 10,
        INTERFACE_TYPE    => "NETWORKING", 
        DYN_CLKDIV_INV_EN => "FALSE",
        DYN_CLK_INV_EN    => "FALSE",
        NUM_CE            => 2,
        OFB_USED          => "FALSE",
        IOBDELAY          => "BOTH",
        SERDES_MODE       => "MASTER")
    port map (
        Q1                => iserdes_q_i(0),
        Q2                => iserdes_q_i(1),
        Q3                => iserdes_q_i(2),
        Q4                => iserdes_q_i(3),
        Q5                => iserdes_q_i(4),
        Q6                => iserdes_q_i(5),
        Q7                => iserdes_q_i(6),
        Q8                => iserdes_q_i(7),
        SHIFTOUT1         => icascade1,               -- Cascade connection to Slave ISERDES
        SHIFTOUT2         => icascade2,               -- Cascade connection to Slave ISERDES
        BITSLIP           => '0',                            -- 1-bit Invoke Bitslip. This can be used with any 
                                                              -- DATA_WIDTH, cascaded or not.
        CE1               => '1',                       -- 1-bit Clock enable input
        CE2               => '1',                       -- 1-bit Clock enable input
        CLK               => clk_in,                             -- Fast clock driven by MMCM
        CLKB              => clk_in_int_inv,                     -- Locally inverted clock
        CLKDIV            => clk_div_in,                         -- Slow clock driven by MMCM
        CLKDIVP           => '0',
        D                 => data_in, -- 1-bit Input signal from IOB.
        DDLY              => data_delayed,
        RST               => io_reset,                           -- 1-bit Asynchronous reset only.
        SHIFTIN1          => '0',
        SHIFTIN2          => '0',
        -- unused connections
        DYNCLKDIVSEL      => '0',
        DYNCLKSEL         => '0',
        OFB               => '0',
        OCLK              => '0',
        OCLKB             => '0',
        O                 => open);                              -- unregistered output of ISERDESE1

iserdese2_slave : ISERDESE2
    generic map (
    DATA_RATE         => "DDR",
    DATA_WIDTH        => 10,
    INTERFACE_TYPE    => "NETWORKING",
    DYN_CLKDIV_INV_EN => "FALSE",
    DYN_CLK_INV_EN    => "FALSE",
    NUM_CE            => 2,
    OFB_USED          => "FALSE",
    IOBDELAY          => "NONE",                             -- Use input at D to output the data on Q1-Q6
    SERDES_MODE       => "SLAVE")
    port map (
    Q1                => open,
    Q2                => open,
    Q3                => iserdes_q_i(8),
    Q4                => iserdes_q_i(9),
    Q5                => iserdes_q_i(10),
    Q6                => iserdes_q_i(11),
    Q7                => iserdes_q_i(12),
    Q8                => iserdes_q_i(13),
    SHIFTOUT1         => open,
    SHIFTOUT2         => open,
    SHIFTIN1          => icascade1,               -- Cascade connections from Master ISERDES
    SHIFTIN2          => icascade2,               -- Cascade connections from Master ISERDES
    BITSLIP           => '0',                            -- 1-bit Invoke Bitslip. This can be used with any 
                                                          -- DATA_WIDTH, cascaded or not.
    CE1               => '1',                       -- 1-bit Clock enable input
    CE2               => '1',                       -- 1-bit Clock enable input
    CLK               => clk_in,                             -- Fast clock driven by MMCM
    CLKB              => clk_in_int_inv,                     -- locally inverted clock
    CLKDIV            => clk_div_in,                         -- Slow clock driven by MMCM
    CLKDIVP           => '0',
    D                 => '0',                                -- Slave ISERDES module. No need to connect D, DDLY
    DDLY              => '0',
    RST               => io_reset,                           -- 1-bit Asynchronous reset only.
    -- unused connections
    DYNCLKDIVSEL      => '0',
    DYNCLKSEL         => '0',
    OFB               => '0',
    OCLK             => '0',
    OCLKB            => '0',
    O                => open);                              -- unregistered output of ISERDESE1
    

end Behavioral;
