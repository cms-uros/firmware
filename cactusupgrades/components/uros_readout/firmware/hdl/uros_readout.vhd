----------------------------------------------------------------------------------
-- Everything related to readout
-- �lvaro Navarro, CIEMAT, 2015
----------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
library UNIMACRO;
use unimacro.Vcomponents.all;



library work;
use work.ipbus.all;
use work.uros_readout_decl.all;
use work.ipbus_reg_types.all;
use work.minmax.all;

use work.mp7_ttc_decl.all;

entity uros_readout is
    port(
        clk: in std_logic; -- ipbus clock, rst, bus
        rst: in std_logic;
        ipb_in: in ipb_wbus;
        ipb_out: out ipb_rbus;

        ttc_clk: in std_logic;
        ttc_rst: in std_logic;
        ttc_cmd: in ttc_cmd_t;
        l1a: in std_logic;
        --bunch_ctr: in bctr_t;
        bunch_ctr_mp7_in: in bctr_t;
        --bunch_ctr_incr: out bctr_t;
        --evt_ctr: out eoctr_t;
        orb_ctr: in eoctr_t;

        amc13_refclk: in std_logic;
        
        -- DT Deser
        LVDS_in_p : in std_logic_vector(71 downto 0);
        LVDS_in_n : in std_logic_vector(71 downto 0);
        
        SN_RXEN_HR   : out std_logic_vector(5 downto 0); 
        SN_SD_HR     : in std_logic_vector(5 downto 0);
        SN_SQEN_HR   : out std_logic_vector(5 downto 0);
        SN_ENSD_HR   : out std_logic_vector(5 downto 0);
        
        clk40  : in std_logic;
        clk80  : in std_logic;
        clk120 : in std_logic;
        clk200 : in std_logic;
        clk600 : in std_logic;
        rst40  : in std_logic;
        
        -- Pins
        AMC_P1_RX_P, AMC_P1_RX_N : in std_logic;
        AMC_P1_TX_N, AMC_P1_TX_P : out std_logic--;
    );
end uros_readout;

architecture Behavioral of uros_readout is

    signal evt_ctr_i: unsigned(eoctr_t'range);
    signal evt_ctr_u, orb_ctr_u: eoctr_t;
    signal bunch_ctr: bctr_t;
    signal resync, ec0: std_logic;

    --L1A FIFO
    constant INTERNAL_TIME_WIDTH : integer := 32;
    constant FIFO_WIDTH : integer := 1 + INTERNAL_TIME_WIDTH + RO_EVN_WIDTH + RO_ORN_WIDTH + RO_BXN_WIDTH;
    constant FIFO_DEPTH : integer := 10;
    signal l1ff_DO, l1ff_DI : std_logic_vector(FIFO_WIDTH-1 downto 0) := (others => '0');
    signal l1ff_RDCOUNT,l1ff_WRCOUNT : std_logic_vector(FIFO_DEPTH-1 downto 0) := (others => '0');
    signal l1ff_EMPTY, l1ff_RDEN, l1ff_WREN, l1ff_AF, l1ff_AE, l1ff_FULL, l1ff_RST: std_logic := '0';
    signal l1ff_do_resync : std_logic := '0';
    signal l1ff_do_time : std_logic_vector(INTERNAL_TIME_WIDTH - 1 downto 0);
    signal l1ff_do_orn : std_logic_vector(RO_ORN_WIDTH - 1 downto 0);
    signal l1ff_do_evn : std_logic_vector(RO_EVN_WIDTH - 1 downto 0);
    signal l1ff_do_bxn : std_logic_vector(RO_BXN_WIDTH - 1 downto 0);

    signal clk_tts  : std_logic;
    signal tts      : std_logic_vector (3 downto 0);
    signal amc13_valid, amc13_hdr, amc13_trl : std_logic;
    signal amc13_d  : std_logic_vector (63 downto 0);
    signal amc13_af, amc13_rdy, amc13_resync : std_logic;
    signal rst_link : std_logic;
    signal internal_time : unsigned(INTERNAL_TIME_WIDTH-1 downto 0);
    
    constant N_STAT : integer := 64;
    constant N_CTRL : integer := 64;
    signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);
    signal stat, csrd: ipb_reg_v(N_STAT-1 downto 0);
    signal conf, csrq: ipb_reg_v(N_CTRL-1 downto 0);
    
    -- DT Deser
    constant N_CHAN : integer := 72;
    signal robff_do         : ipb_reg_v(N_CHAN-1 downto 0);
    signal robff_empty      : std_logic_vector(N_CHAN-1 downto 0);
    signal robff_rdclk      : std_logic;
    signal robff_rden       : std_logic_vector(N_CHAN-1 downto 0);
    signal robstats         : rrstats_v_t(N_CHAN-1 downto 0);

    function rsv2ctrinc (rs: rrstats_v_t(N_CHAN-1 downto 0); index: natural) return std_logic_vector(N_CHAN-1 downto 0) is
        variable result : std_logic_vector(N_CHAN-1 downto 0);
    begin
        for i in result'range loop
            result(i) := rs(i).counters_inc(index);
        end loop;
    return result;
    end function rsv2ctrinc;

    signal ctrinc_header_bxnbad : std_logic_vector(N_CHAN - 1 downto 0);
    signal ctrinc_toomuchdata   : std_logic_vector(N_CHAN - 1 downto 0);
    signal ctrinc_headerlate    : std_logic_vector(N_CHAN - 1 downto 0);
    signal ctrinc_headerpurged  : std_logic_vector(N_CHAN - 1 downto 0);
    signal ctrinc_hits2daq      : std_logic_vector(N_CHAN - 1 downto 0);
    signal ctrinc_trailer2daq   : std_logic_vector(N_CHAN - 1 downto 0);
    signal ctrinc_trailer_read  : std_logic_vector(N_CHAN - 1 downto 0);
    
    constant DUMMYDATA_LOG2_DIV : integer := 3 ; -- w**DUMMYDATA_DIV =  dummydata generator resolution
    
    --------------------------------------------------------------------------------
    -- Start of automatically-generated VHDL code for register "breakout" signals declaration
    signal conf_amc13_rst_edge, conf_amc13_rst_hold, conf_ctr_rst, conf_dteb_enforce_bxn, conf_dteb_rob_evn_inc,
        conf_rob_ctr_ena, conf_rob_debugmem_advance, conf_rob_debugmem_start, conf_snap12_ensd_use,
        conf_snap12_rxen_use, conf_snap12_sqen_use, conf_stat_freeze, stat_amc13_af, stat_amc13_rdy, stat_l1afifo_ae,
        stat_l1afifo_af, stat_l1afifo_empty, stat_l1afifo_full, stat_l1afifo_rden, stat_l1afifo_resync,
        stat_l1afifo_wren, stat_rob_nowlock, stat_tmp_purge_last : std_logic := '0';


    signal stat_TTS, stat_evb_fsm_state : std_logic_vector(3 downto 0) := (others => '0');
    signal conf_snap12_ensd, conf_snap12_rxen, conf_snap12_sqen,
        stat_snap12_sd : std_logic_vector(5 downto 0) := (others => '0');
    signal conf_bcn_incr, conf_dteb_status_mask, stat_l1afifo_bxn, stat_rob_word12_data, stat_rob_word12_weak,
        stat_tmp_purge_evn : std_logic_vector(11 downto 0) := (others => '0');
    signal conf_boardID, stat_l1afifo_orn : std_logic_vector(15 downto 0) := (others => '0');
    signal stat_l1afifo_evn : std_logic_vector(23 downto 0) := (others => '0');
    signal stat_rob_samples_3 : std_logic_vector(26 downto 0) := (others => '0');
    signal conf_userData, stat_gth, stat_rob_samples_1, stat_rob_samples_2,
        stat_tmp_robff_empty : std_logic_vector(31 downto 0) := (others => '0');
    signal conf_dteb_disable_ch : std_logic_vector(71 downto 0) := (others => '0');
    signal stat_debugmem_port : std_logic_vector(127 downto 0) := (others => '0');
    signal conf_rob_pd_inertia : std_logic_vector(215 downto 0) := (others => '0');
    signal conf_rob_idelay : std_logic_vector(359 downto 0) := (others => '0');
    signal conf_rob_pd_decay : std_logic_vector(575 downto 0) := (others => '0');

    signal conf_amc13_rst_len : unsigned(3 downto 0) := (others => '0');
    signal conf_rob_debugmem_trig : unsigned(4 downto 0) := (others => '0');
    signal conf_rob_max_inevent_idles : unsigned(5 downto 0) := (others => '0');
    signal conf_rob_debugmem_mux, conf_rob_stat_mux : unsigned(6 downto 0) := (others => '0');
    signal conf_dteb_dummydata_mul, conf_dteb_max_words_per_ch, stat_resync_queued, stat_resync_queued_max,
        stat_rob_netedgeshift, stat_rob_startstoperr_ctr : unsigned(7 downto 0) := (others => '0');
    signal stat_l1afifo_occupancy, stat_l1afifo_occupancy_max : unsigned(9 downto 0) := (others => '0');
    signal conf_rob_af_thres, stat_rob_max_ff_occupancy : unsigned(10 downto 0) := (others => '0');
    signal conf_dteb_big_evt_size_ctr_threshold, conf_dteb_max_words_per_ev, stat_rob_edgeshift_ctr,
        stat_rob_unlock12_ctr : unsigned(11 downto 0) := (others => '0');
    signal conf_dteb_max_hdr_delay, conf_tts_thresholds_bsy2ofw, conf_tts_thresholds_ofw2bsy,
        conf_tts_thresholds_ofw2rdy, conf_tts_thresholds_rdy2ofw, stat_big_evt_size_ctr, stat_max_evt_size_ctr,
        stat_rob_badtrailer_evn_ctr, stat_rob_badtrailer_wrd_ctr, stat_rob_errs_ctr, stat_rob_fifo_ofw_losses_ctr,
        stat_rob_header_bxnbad_ctr, stat_rob_headerlate_ctr, stat_rob_headerpurged_ctr, stat_rob_inevent_idles_ctr,
        stat_rob_nonconsecutive_ctr, stat_rob_not_in_event_ctr, stat_rob_notrailer_ctr, stat_rob_parityerr_nofix_ctr,
        stat_rob_parityerr_total_ctr, stat_rob_toomuchdata_ctr, stat_rob_unknown_robword_ctr,
        stat_rob_w32interrupt_ctr, stat_tmp_dt_time, stat_tmp_purge_time : unsigned(15 downto 0) := (others => '0');
    signal stat_amc13_reset_ctr, stat_rob_header_ctr, stat_rob_hits2daq_ctr, stat_rob_hits_ctr,
        stat_rob_trailer2daq_ctr, stat_rob_trailer_ctr, stat_rob_trailer_read_ctr, stat_rob_valid_ctr,
        stat_rob_w32valid_ctr, stat_rob_wren_ctr, stat_tmp_timesupdiff : unsigned(31 downto 0) := (others => '0');
    signal stat_amc13_af_ctr, stat_amc13_nrdy_ctr, stat_amc13_rdy_ctr, stat_header_ctr, stat_l1a_ctr, stat_resync_ctr,
        stat_rob_fifo_af_ctr, stat_trailer_ctr, stat_tts_bsy_ctr, stat_tts_ofw_ctr, stat_tts_rdy_ctr,
        stat_valid_ctr : unsigned(47 downto 0) := (others => '0');

    -- End of automatically-generated VHDL code for register "breakout" signals declaration
    --------------------------------------------------------------------------------




begin




    --------------------------------------------------------------------------------
    -- Start of automatically-generated VHDL code for register "breakout" signals assignment
    conf_userData                        <= conf(16#00#)(31 downto 0);
    conf_boardID                         <= conf(16#01#)(15 downto 0);
    conf_bcn_incr                        <= conf(16#01#)(27 downto 16);
    conf_stat_freeze                     <= conf(16#01#)(28);
    conf_ctr_rst                         <= conf(16#01#)(29);
    conf_amc13_rst_len                   <= unsigned( conf(16#02#)(3 downto 0) );
    conf_amc13_rst_edge                  <= conf(16#02#)(4);
    conf_amc13_rst_hold                  <= conf(16#02#)(5);
    conf_tts_thresholds_ofw2rdy          <= unsigned( conf(16#03#)(15 downto 0) );
    conf_tts_thresholds_rdy2ofw          <= unsigned( conf(16#03#)(31 downto 16) );
    conf_tts_thresholds_bsy2ofw          <= unsigned( conf(16#04#)(15 downto 0) );
    conf_tts_thresholds_ofw2bsy          <= unsigned( conf(16#04#)(31 downto 16) );
    conf_rob_pd_inertia(2 downto 0)      <= conf(16#05#)(2 downto 0);
    conf_rob_pd_inertia(5 downto 3)      <= conf(16#05#)(18 downto 16);
    conf_rob_pd_inertia(8 downto 6)      <= conf(16#06#)(2 downto 0);
    conf_rob_pd_inertia(11 downto 9)     <= conf(16#06#)(18 downto 16);
    conf_rob_pd_inertia(14 downto 12)    <= conf(16#07#)(2 downto 0);
    conf_rob_pd_inertia(17 downto 15)    <= conf(16#07#)(18 downto 16);
    conf_rob_pd_inertia(20 downto 18)    <= conf(16#08#)(2 downto 0);
    conf_rob_pd_inertia(23 downto 21)    <= conf(16#08#)(18 downto 16);
    conf_rob_pd_inertia(26 downto 24)    <= conf(16#09#)(2 downto 0);
    conf_rob_pd_inertia(29 downto 27)    <= conf(16#09#)(18 downto 16);
    conf_rob_pd_inertia(32 downto 30)    <= conf(16#0A#)(2 downto 0);
    conf_rob_pd_inertia(35 downto 33)    <= conf(16#0A#)(18 downto 16);
    conf_rob_pd_inertia(38 downto 36)    <= conf(16#0B#)(2 downto 0);
    conf_rob_pd_inertia(41 downto 39)    <= conf(16#0B#)(18 downto 16);
    conf_rob_pd_inertia(44 downto 42)    <= conf(16#0C#)(2 downto 0);
    conf_rob_pd_inertia(47 downto 45)    <= conf(16#0C#)(18 downto 16);
    conf_rob_pd_inertia(50 downto 48)    <= conf(16#0D#)(2 downto 0);
    conf_rob_pd_inertia(53 downto 51)    <= conf(16#0D#)(18 downto 16);
    conf_rob_pd_inertia(56 downto 54)    <= conf(16#0E#)(2 downto 0);
    conf_rob_pd_inertia(59 downto 57)    <= conf(16#0E#)(18 downto 16);
    conf_rob_pd_inertia(62 downto 60)    <= conf(16#0F#)(2 downto 0);
    conf_rob_pd_inertia(65 downto 63)    <= conf(16#0F#)(18 downto 16);
    conf_rob_pd_inertia(68 downto 66)    <= conf(16#10#)(2 downto 0);
    conf_rob_pd_inertia(71 downto 69)    <= conf(16#10#)(18 downto 16);
    conf_rob_pd_inertia(74 downto 72)    <= conf(16#11#)(2 downto 0);
    conf_rob_pd_inertia(77 downto 75)    <= conf(16#11#)(18 downto 16);
    conf_rob_pd_inertia(80 downto 78)    <= conf(16#12#)(2 downto 0);
    conf_rob_pd_inertia(83 downto 81)    <= conf(16#12#)(18 downto 16);
    conf_rob_pd_inertia(86 downto 84)    <= conf(16#13#)(2 downto 0);
    conf_rob_pd_inertia(89 downto 87)    <= conf(16#13#)(18 downto 16);
    conf_rob_pd_inertia(92 downto 90)    <= conf(16#14#)(2 downto 0);
    conf_rob_pd_inertia(95 downto 93)    <= conf(16#14#)(18 downto 16);
    conf_rob_pd_inertia(98 downto 96)    <= conf(16#15#)(2 downto 0);
    conf_rob_pd_inertia(101 downto 99)   <= conf(16#15#)(18 downto 16);
    conf_rob_pd_inertia(104 downto 102)  <= conf(16#16#)(2 downto 0);
    conf_rob_pd_inertia(107 downto 105)  <= conf(16#16#)(18 downto 16);
    conf_rob_pd_inertia(110 downto 108)  <= conf(16#17#)(2 downto 0);
    conf_rob_pd_inertia(113 downto 111)  <= conf(16#17#)(18 downto 16);
    conf_rob_pd_inertia(116 downto 114)  <= conf(16#18#)(2 downto 0);
    conf_rob_pd_inertia(119 downto 117)  <= conf(16#18#)(18 downto 16);
    conf_rob_pd_inertia(122 downto 120)  <= conf(16#19#)(2 downto 0);
    conf_rob_pd_inertia(125 downto 123)  <= conf(16#19#)(18 downto 16);
    conf_rob_pd_inertia(128 downto 126)  <= conf(16#1A#)(2 downto 0);
    conf_rob_pd_inertia(131 downto 129)  <= conf(16#1A#)(18 downto 16);
    conf_rob_pd_inertia(134 downto 132)  <= conf(16#1B#)(2 downto 0);
    conf_rob_pd_inertia(137 downto 135)  <= conf(16#1B#)(18 downto 16);
    conf_rob_pd_inertia(140 downto 138)  <= conf(16#1C#)(2 downto 0);
    conf_rob_pd_inertia(143 downto 141)  <= conf(16#1C#)(18 downto 16);
    conf_rob_pd_inertia(146 downto 144)  <= conf(16#1D#)(2 downto 0);
    conf_rob_pd_inertia(149 downto 147)  <= conf(16#1D#)(18 downto 16);
    conf_rob_pd_inertia(152 downto 150)  <= conf(16#1E#)(2 downto 0);
    conf_rob_pd_inertia(155 downto 153)  <= conf(16#1E#)(18 downto 16);
    conf_rob_pd_inertia(158 downto 156)  <= conf(16#1F#)(2 downto 0);
    conf_rob_pd_inertia(161 downto 159)  <= conf(16#1F#)(18 downto 16);
    conf_rob_pd_inertia(164 downto 162)  <= conf(16#20#)(2 downto 0);
    conf_rob_pd_inertia(167 downto 165)  <= conf(16#20#)(18 downto 16);
    conf_rob_pd_inertia(170 downto 168)  <= conf(16#21#)(2 downto 0);
    conf_rob_pd_inertia(173 downto 171)  <= conf(16#21#)(18 downto 16);
    conf_rob_pd_inertia(176 downto 174)  <= conf(16#22#)(2 downto 0);
    conf_rob_pd_inertia(179 downto 177)  <= conf(16#22#)(18 downto 16);
    conf_rob_pd_inertia(182 downto 180)  <= conf(16#23#)(2 downto 0);
    conf_rob_pd_inertia(185 downto 183)  <= conf(16#23#)(18 downto 16);
    conf_rob_pd_inertia(188 downto 186)  <= conf(16#24#)(2 downto 0);
    conf_rob_pd_inertia(191 downto 189)  <= conf(16#24#)(18 downto 16);
    conf_rob_pd_inertia(194 downto 192)  <= conf(16#25#)(2 downto 0);
    conf_rob_pd_inertia(197 downto 195)  <= conf(16#25#)(18 downto 16);
    conf_rob_pd_inertia(200 downto 198)  <= conf(16#26#)(2 downto 0);
    conf_rob_pd_inertia(203 downto 201)  <= conf(16#26#)(18 downto 16);
    conf_rob_pd_inertia(206 downto 204)  <= conf(16#27#)(2 downto 0);
    conf_rob_pd_inertia(209 downto 207)  <= conf(16#27#)(18 downto 16);
    conf_rob_pd_inertia(212 downto 210)  <= conf(16#28#)(2 downto 0);
    conf_rob_pd_inertia(215 downto 213)  <= conf(16#28#)(18 downto 16);
    conf_rob_pd_decay(7 downto 0)        <= conf(16#05#)(10 downto 3);
    conf_rob_pd_decay(15 downto 8)       <= conf(16#05#)(26 downto 19);
    conf_rob_pd_decay(23 downto 16)      <= conf(16#06#)(10 downto 3);
    conf_rob_pd_decay(31 downto 24)      <= conf(16#06#)(26 downto 19);
    conf_rob_pd_decay(39 downto 32)      <= conf(16#07#)(10 downto 3);
    conf_rob_pd_decay(47 downto 40)      <= conf(16#07#)(26 downto 19);
    conf_rob_pd_decay(55 downto 48)      <= conf(16#08#)(10 downto 3);
    conf_rob_pd_decay(63 downto 56)      <= conf(16#08#)(26 downto 19);
    conf_rob_pd_decay(71 downto 64)      <= conf(16#09#)(10 downto 3);
    conf_rob_pd_decay(79 downto 72)      <= conf(16#09#)(26 downto 19);
    conf_rob_pd_decay(87 downto 80)      <= conf(16#0A#)(10 downto 3);
    conf_rob_pd_decay(95 downto 88)      <= conf(16#0A#)(26 downto 19);
    conf_rob_pd_decay(103 downto 96)     <= conf(16#0B#)(10 downto 3);
    conf_rob_pd_decay(111 downto 104)    <= conf(16#0B#)(26 downto 19);
    conf_rob_pd_decay(119 downto 112)    <= conf(16#0C#)(10 downto 3);
    conf_rob_pd_decay(127 downto 120)    <= conf(16#0C#)(26 downto 19);
    conf_rob_pd_decay(135 downto 128)    <= conf(16#0D#)(10 downto 3);
    conf_rob_pd_decay(143 downto 136)    <= conf(16#0D#)(26 downto 19);
    conf_rob_pd_decay(151 downto 144)    <= conf(16#0E#)(10 downto 3);
    conf_rob_pd_decay(159 downto 152)    <= conf(16#0E#)(26 downto 19);
    conf_rob_pd_decay(167 downto 160)    <= conf(16#0F#)(10 downto 3);
    conf_rob_pd_decay(175 downto 168)    <= conf(16#0F#)(26 downto 19);
    conf_rob_pd_decay(183 downto 176)    <= conf(16#10#)(10 downto 3);
    conf_rob_pd_decay(191 downto 184)    <= conf(16#10#)(26 downto 19);
    conf_rob_pd_decay(199 downto 192)    <= conf(16#11#)(10 downto 3);
    conf_rob_pd_decay(207 downto 200)    <= conf(16#11#)(26 downto 19);
    conf_rob_pd_decay(215 downto 208)    <= conf(16#12#)(10 downto 3);
    conf_rob_pd_decay(223 downto 216)    <= conf(16#12#)(26 downto 19);
    conf_rob_pd_decay(231 downto 224)    <= conf(16#13#)(10 downto 3);
    conf_rob_pd_decay(239 downto 232)    <= conf(16#13#)(26 downto 19);
    conf_rob_pd_decay(247 downto 240)    <= conf(16#14#)(10 downto 3);
    conf_rob_pd_decay(255 downto 248)    <= conf(16#14#)(26 downto 19);
    conf_rob_pd_decay(263 downto 256)    <= conf(16#15#)(10 downto 3);
    conf_rob_pd_decay(271 downto 264)    <= conf(16#15#)(26 downto 19);
    conf_rob_pd_decay(279 downto 272)    <= conf(16#16#)(10 downto 3);
    conf_rob_pd_decay(287 downto 280)    <= conf(16#16#)(26 downto 19);
    conf_rob_pd_decay(295 downto 288)    <= conf(16#17#)(10 downto 3);
    conf_rob_pd_decay(303 downto 296)    <= conf(16#17#)(26 downto 19);
    conf_rob_pd_decay(311 downto 304)    <= conf(16#18#)(10 downto 3);
    conf_rob_pd_decay(319 downto 312)    <= conf(16#18#)(26 downto 19);
    conf_rob_pd_decay(327 downto 320)    <= conf(16#19#)(10 downto 3);
    conf_rob_pd_decay(335 downto 328)    <= conf(16#19#)(26 downto 19);
    conf_rob_pd_decay(343 downto 336)    <= conf(16#1A#)(10 downto 3);
    conf_rob_pd_decay(351 downto 344)    <= conf(16#1A#)(26 downto 19);
    conf_rob_pd_decay(359 downto 352)    <= conf(16#1B#)(10 downto 3);
    conf_rob_pd_decay(367 downto 360)    <= conf(16#1B#)(26 downto 19);
    conf_rob_pd_decay(375 downto 368)    <= conf(16#1C#)(10 downto 3);
    conf_rob_pd_decay(383 downto 376)    <= conf(16#1C#)(26 downto 19);
    conf_rob_pd_decay(391 downto 384)    <= conf(16#1D#)(10 downto 3);
    conf_rob_pd_decay(399 downto 392)    <= conf(16#1D#)(26 downto 19);
    conf_rob_pd_decay(407 downto 400)    <= conf(16#1E#)(10 downto 3);
    conf_rob_pd_decay(415 downto 408)    <= conf(16#1E#)(26 downto 19);
    conf_rob_pd_decay(423 downto 416)    <= conf(16#1F#)(10 downto 3);
    conf_rob_pd_decay(431 downto 424)    <= conf(16#1F#)(26 downto 19);
    conf_rob_pd_decay(439 downto 432)    <= conf(16#20#)(10 downto 3);
    conf_rob_pd_decay(447 downto 440)    <= conf(16#20#)(26 downto 19);
    conf_rob_pd_decay(455 downto 448)    <= conf(16#21#)(10 downto 3);
    conf_rob_pd_decay(463 downto 456)    <= conf(16#21#)(26 downto 19);
    conf_rob_pd_decay(471 downto 464)    <= conf(16#22#)(10 downto 3);
    conf_rob_pd_decay(479 downto 472)    <= conf(16#22#)(26 downto 19);
    conf_rob_pd_decay(487 downto 480)    <= conf(16#23#)(10 downto 3);
    conf_rob_pd_decay(495 downto 488)    <= conf(16#23#)(26 downto 19);
    conf_rob_pd_decay(503 downto 496)    <= conf(16#24#)(10 downto 3);
    conf_rob_pd_decay(511 downto 504)    <= conf(16#24#)(26 downto 19);
    conf_rob_pd_decay(519 downto 512)    <= conf(16#25#)(10 downto 3);
    conf_rob_pd_decay(527 downto 520)    <= conf(16#25#)(26 downto 19);
    conf_rob_pd_decay(535 downto 528)    <= conf(16#26#)(10 downto 3);
    conf_rob_pd_decay(543 downto 536)    <= conf(16#26#)(26 downto 19);
    conf_rob_pd_decay(551 downto 544)    <= conf(16#27#)(10 downto 3);
    conf_rob_pd_decay(559 downto 552)    <= conf(16#27#)(26 downto 19);
    conf_rob_pd_decay(567 downto 560)    <= conf(16#28#)(10 downto 3);
    conf_rob_pd_decay(575 downto 568)    <= conf(16#28#)(26 downto 19);
    conf_rob_idelay(4 downto 0)          <= conf(16#05#)(15 downto 11);
    conf_rob_idelay(9 downto 5)          <= conf(16#05#)(31 downto 27);
    conf_rob_idelay(14 downto 10)        <= conf(16#06#)(15 downto 11);
    conf_rob_idelay(19 downto 15)        <= conf(16#06#)(31 downto 27);
    conf_rob_idelay(24 downto 20)        <= conf(16#07#)(15 downto 11);
    conf_rob_idelay(29 downto 25)        <= conf(16#07#)(31 downto 27);
    conf_rob_idelay(34 downto 30)        <= conf(16#08#)(15 downto 11);
    conf_rob_idelay(39 downto 35)        <= conf(16#08#)(31 downto 27);
    conf_rob_idelay(44 downto 40)        <= conf(16#09#)(15 downto 11);
    conf_rob_idelay(49 downto 45)        <= conf(16#09#)(31 downto 27);
    conf_rob_idelay(54 downto 50)        <= conf(16#0A#)(15 downto 11);
    conf_rob_idelay(59 downto 55)        <= conf(16#0A#)(31 downto 27);
    conf_rob_idelay(64 downto 60)        <= conf(16#0B#)(15 downto 11);
    conf_rob_idelay(69 downto 65)        <= conf(16#0B#)(31 downto 27);
    conf_rob_idelay(74 downto 70)        <= conf(16#0C#)(15 downto 11);
    conf_rob_idelay(79 downto 75)        <= conf(16#0C#)(31 downto 27);
    conf_rob_idelay(84 downto 80)        <= conf(16#0D#)(15 downto 11);
    conf_rob_idelay(89 downto 85)        <= conf(16#0D#)(31 downto 27);
    conf_rob_idelay(94 downto 90)        <= conf(16#0E#)(15 downto 11);
    conf_rob_idelay(99 downto 95)        <= conf(16#0E#)(31 downto 27);
    conf_rob_idelay(104 downto 100)      <= conf(16#0F#)(15 downto 11);
    conf_rob_idelay(109 downto 105)      <= conf(16#0F#)(31 downto 27);
    conf_rob_idelay(114 downto 110)      <= conf(16#10#)(15 downto 11);
    conf_rob_idelay(119 downto 115)      <= conf(16#10#)(31 downto 27);
    conf_rob_idelay(124 downto 120)      <= conf(16#11#)(15 downto 11);
    conf_rob_idelay(129 downto 125)      <= conf(16#11#)(31 downto 27);
    conf_rob_idelay(134 downto 130)      <= conf(16#12#)(15 downto 11);
    conf_rob_idelay(139 downto 135)      <= conf(16#12#)(31 downto 27);
    conf_rob_idelay(144 downto 140)      <= conf(16#13#)(15 downto 11);
    conf_rob_idelay(149 downto 145)      <= conf(16#13#)(31 downto 27);
    conf_rob_idelay(154 downto 150)      <= conf(16#14#)(15 downto 11);
    conf_rob_idelay(159 downto 155)      <= conf(16#14#)(31 downto 27);
    conf_rob_idelay(164 downto 160)      <= conf(16#15#)(15 downto 11);
    conf_rob_idelay(169 downto 165)      <= conf(16#15#)(31 downto 27);
    conf_rob_idelay(174 downto 170)      <= conf(16#16#)(15 downto 11);
    conf_rob_idelay(179 downto 175)      <= conf(16#16#)(31 downto 27);
    conf_rob_idelay(184 downto 180)      <= conf(16#17#)(15 downto 11);
    conf_rob_idelay(189 downto 185)      <= conf(16#17#)(31 downto 27);
    conf_rob_idelay(194 downto 190)      <= conf(16#18#)(15 downto 11);
    conf_rob_idelay(199 downto 195)      <= conf(16#18#)(31 downto 27);
    conf_rob_idelay(204 downto 200)      <= conf(16#19#)(15 downto 11);
    conf_rob_idelay(209 downto 205)      <= conf(16#19#)(31 downto 27);
    conf_rob_idelay(214 downto 210)      <= conf(16#1A#)(15 downto 11);
    conf_rob_idelay(219 downto 215)      <= conf(16#1A#)(31 downto 27);
    conf_rob_idelay(224 downto 220)      <= conf(16#1B#)(15 downto 11);
    conf_rob_idelay(229 downto 225)      <= conf(16#1B#)(31 downto 27);
    conf_rob_idelay(234 downto 230)      <= conf(16#1C#)(15 downto 11);
    conf_rob_idelay(239 downto 235)      <= conf(16#1C#)(31 downto 27);
    conf_rob_idelay(244 downto 240)      <= conf(16#1D#)(15 downto 11);
    conf_rob_idelay(249 downto 245)      <= conf(16#1D#)(31 downto 27);
    conf_rob_idelay(254 downto 250)      <= conf(16#1E#)(15 downto 11);
    conf_rob_idelay(259 downto 255)      <= conf(16#1E#)(31 downto 27);
    conf_rob_idelay(264 downto 260)      <= conf(16#1F#)(15 downto 11);
    conf_rob_idelay(269 downto 265)      <= conf(16#1F#)(31 downto 27);
    conf_rob_idelay(274 downto 270)      <= conf(16#20#)(15 downto 11);
    conf_rob_idelay(279 downto 275)      <= conf(16#20#)(31 downto 27);
    conf_rob_idelay(284 downto 280)      <= conf(16#21#)(15 downto 11);
    conf_rob_idelay(289 downto 285)      <= conf(16#21#)(31 downto 27);
    conf_rob_idelay(294 downto 290)      <= conf(16#22#)(15 downto 11);
    conf_rob_idelay(299 downto 295)      <= conf(16#22#)(31 downto 27);
    conf_rob_idelay(304 downto 300)      <= conf(16#23#)(15 downto 11);
    conf_rob_idelay(309 downto 305)      <= conf(16#23#)(31 downto 27);
    conf_rob_idelay(314 downto 310)      <= conf(16#24#)(15 downto 11);
    conf_rob_idelay(319 downto 315)      <= conf(16#24#)(31 downto 27);
    conf_rob_idelay(324 downto 320)      <= conf(16#25#)(15 downto 11);
    conf_rob_idelay(329 downto 325)      <= conf(16#25#)(31 downto 27);
    conf_rob_idelay(334 downto 330)      <= conf(16#26#)(15 downto 11);
    conf_rob_idelay(339 downto 335)      <= conf(16#26#)(31 downto 27);
    conf_rob_idelay(344 downto 340)      <= conf(16#27#)(15 downto 11);
    conf_rob_idelay(349 downto 345)      <= conf(16#27#)(31 downto 27);
    conf_rob_idelay(354 downto 350)      <= conf(16#28#)(15 downto 11);
    conf_rob_idelay(359 downto 355)      <= conf(16#28#)(31 downto 27);
    conf_rob_ctr_ena                     <= conf(16#29#)(0);
    conf_rob_max_inevent_idles           <= unsigned( conf(16#29#)(6 downto 1) );
    conf_rob_stat_mux                    <= unsigned( conf(16#29#)(13 downto 7) );
    conf_rob_af_thres                    <= unsigned( conf(16#29#)(24 downto 14) );
    conf_rob_debugmem_mux                <= unsigned( conf(16#2A#)(6 downto 0) );
    conf_rob_debugmem_trig               <= unsigned( conf(16#2A#)(11 downto 7) );
    conf_rob_debugmem_start              <= conf(16#2A#)(12);
    conf_rob_debugmem_advance            <= conf(16#2A#)(13);
    conf_snap12_rxen                     <= conf(16#30#)(5 downto 0);
    conf_snap12_rxen_use                 <= conf(16#30#)(6);
    conf_snap12_sqen                     <= conf(16#30#)(13 downto 8);
    conf_snap12_sqen_use                 <= conf(16#30#)(14);
    conf_snap12_ensd                     <= conf(16#30#)(21 downto 16);
    conf_snap12_ensd_use                 <= conf(16#30#)(22);
    conf_dteb_disable_ch(11 downto 0)    <= conf(16#31#)(11 downto 0);
    conf_dteb_disable_ch(23 downto 12)   <= conf(16#31#)(23 downto 12);
    conf_dteb_disable_ch(35 downto 24)   <= conf(16#32#)(11 downto 0);
    conf_dteb_disable_ch(47 downto 36)   <= conf(16#32#)(23 downto 12);
    conf_dteb_disable_ch(59 downto 48)   <= conf(16#33#)(11 downto 0);
    conf_dteb_disable_ch(71 downto 60)   <= conf(16#33#)(23 downto 12);
    conf_dteb_dummydata_mul              <= unsigned( conf(16#34#)(7 downto 0) );
    conf_dteb_max_words_per_ch           <= unsigned( conf(16#34#)(15 downto 8) );
    conf_dteb_rob_evn_inc                <= conf(16#34#)(16);
    conf_dteb_enforce_bxn                <= conf(16#34#)(17);
    conf_dteb_max_words_per_ev           <= unsigned( conf(16#35#)(11 downto 0) );
    conf_dteb_big_evt_size_ctr_threshold <= unsigned( conf(16#35#)(23 downto 12) );
    conf_dteb_max_hdr_delay              <= unsigned( conf(16#36#)(15 downto 0) );
    conf_dteb_status_mask                <= conf(16#36#)(27 downto 16);
    stat(16#00#)(31 downto 0)            <= std_logic_vector( stat_resync_ctr(31 downto 0) );
    stat(16#01#)(15 downto 0)            <= std_logic_vector( stat_resync_ctr(47 downto 32) );
    stat(16#01#)(23 downto 16)           <= std_logic_vector( stat_resync_queued );
    stat(16#01#)(31 downto 24)           <= std_logic_vector( stat_resync_queued_max );
    stat(16#02#)(31 downto 0)            <= std_logic_vector( stat_l1a_ctr(31 downto 0) );
    stat(16#03#)(15 downto 0)            <= std_logic_vector( stat_l1a_ctr(47 downto 32) );
    stat(16#04#)(31 downto 0)            <= std_logic_vector( stat_header_ctr(31 downto 0) );
    stat(16#05#)(15 downto 0)            <= std_logic_vector( stat_header_ctr(47 downto 32) );
    stat(16#06#)(31 downto 0)            <= std_logic_vector( stat_trailer_ctr(31 downto 0) );
    stat(16#07#)(15 downto 0)            <= std_logic_vector( stat_trailer_ctr(47 downto 32) );
    stat(16#08#)(31 downto 0)            <= std_logic_vector( stat_valid_ctr(31 downto 0) );
    stat(16#09#)(15 downto 0)            <= std_logic_vector( stat_valid_ctr(47 downto 32) );
    stat(16#09#)(21 downto 16)           <= stat_snap12_sd;
    stat(16#0A#)(15 downto 0)            <= std_logic_vector( stat_max_evt_size_ctr );
    stat(16#0A#)(31 downto 16)           <= std_logic_vector( stat_big_evt_size_ctr );
    stat(16#0B#)(3 downto 0)             <= stat_TTS;
    stat(16#0B#)(7 downto 4)             <= stat_evb_fsm_state;
    stat(16#0C#)(31 downto 0)            <= std_logic_vector( stat_tts_rdy_ctr(31 downto 0) );
    stat(16#0D#)(15 downto 0)            <= std_logic_vector( stat_tts_rdy_ctr(47 downto 32) );
    stat(16#0E#)(31 downto 0)            <= std_logic_vector( stat_tts_ofw_ctr(31 downto 0) );
    stat(16#0F#)(15 downto 0)            <= std_logic_vector( stat_tts_ofw_ctr(47 downto 32) );
    stat(16#10#)(31 downto 0)            <= std_logic_vector( stat_tts_bsy_ctr(31 downto 0) );
    stat(16#11#)(15 downto 0)            <= std_logic_vector( stat_tts_bsy_ctr(47 downto 32) );
    stat(16#12#)(9 downto 0)             <= std_logic_vector( stat_l1afifo_occupancy );
    stat(16#12#)(23 downto 14)           <= std_logic_vector( stat_l1afifo_occupancy_max );
    stat(16#13#)(23 downto 0)            <= stat_l1afifo_evn;
    stat(16#13#)(24)                     <= stat_l1afifo_empty;
    stat(16#13#)(25)                     <= stat_l1afifo_rden;
    stat(16#13#)(26)                     <= stat_l1afifo_ae;
    stat(16#13#)(27)                     <= stat_l1afifo_full;
    stat(16#13#)(28)                     <= stat_l1afifo_af;
    stat(16#13#)(29)                     <= stat_l1afifo_wren;
    stat(16#14#)(15 downto 0)            <= stat_l1afifo_orn;
    stat(16#14#)(27 downto 16)           <= stat_l1afifo_bxn;
    stat(16#14#)(28)                     <= stat_l1afifo_resync;
    stat(16#15#)(31 downto 0)            <= std_logic_vector( stat_tmp_timesupdiff );
    stat(16#16#)(15 downto 0)            <= std_logic_vector( stat_tmp_dt_time );
    stat(16#16#)(31 downto 16)           <= std_logic_vector( stat_tmp_purge_time );
    stat(16#17#)(31 downto 0)            <= stat_tmp_robff_empty;
    stat(16#18#)(11 downto 0)            <= stat_tmp_purge_evn;
    stat(16#18#)(12)                     <= stat_tmp_purge_last;
    stat(16#19#)(0)                      <= stat_amc13_rdy;
    stat(16#19#)(1)                      <= stat_amc13_af;
    stat(16#1A#)(31 downto 0)            <= std_logic_vector( stat_amc13_reset_ctr );
    stat(16#1B#)(31 downto 0)            <= std_logic_vector( stat_amc13_rdy_ctr(31 downto 0) );
    stat(16#1C#)(15 downto 0)            <= std_logic_vector( stat_amc13_rdy_ctr(47 downto 32) );
    stat(16#1D#)(31 downto 0)            <= std_logic_vector( stat_amc13_nrdy_ctr(31 downto 0) );
    stat(16#1E#)(15 downto 0)            <= std_logic_vector( stat_amc13_nrdy_ctr(47 downto 32) );
    stat(16#1F#)(31 downto 0)            <= std_logic_vector( stat_amc13_af_ctr(31 downto 0) );
    stat(16#20#)(15 downto 0)            <= std_logic_vector( stat_amc13_af_ctr(47 downto 32) );
    stat(16#21#)(31 downto 0)            <= stat_gth;
    stat(16#22#)(31 downto 0)            <= stat_rob_samples_1;
    stat(16#23#)(31 downto 0)            <= stat_rob_samples_2;
    stat(16#24#)(26 downto 0)            <= stat_rob_samples_3;
    stat(16#24#)(31)                     <= stat_rob_nowlock;
    stat(16#25#)(11 downto 0)            <= stat_rob_word12_data;
    stat(16#25#)(23 downto 12)           <= stat_rob_word12_weak;
    stat(16#25#)(31 downto 24)           <= std_logic_vector( stat_rob_netedgeshift );
    stat(16#26#)(10 downto 0)            <= std_logic_vector( stat_rob_max_ff_occupancy );
    stat(16#27#)(11 downto 0)            <= std_logic_vector( stat_rob_edgeshift_ctr );
    stat(16#27#)(23 downto 12)           <= std_logic_vector( stat_rob_unlock12_ctr );
    stat(16#27#)(31 downto 24)           <= std_logic_vector( stat_rob_startstoperr_ctr );
    stat(16#28#)(15 downto 0)            <= std_logic_vector( stat_rob_parityerr_total_ctr );
    stat(16#28#)(31 downto 16)           <= std_logic_vector( stat_rob_parityerr_nofix_ctr );
    stat(16#29#)(31 downto 0)            <= std_logic_vector( stat_rob_valid_ctr );
    stat(16#2A#)(31 downto 0)            <= std_logic_vector( stat_rob_w32valid_ctr );
    stat(16#2B#)(31 downto 0)            <= std_logic_vector( stat_rob_header_ctr );
    stat(16#2C#)(31 downto 0)            <= std_logic_vector( stat_rob_trailer_ctr );
    stat(16#2D#)(31 downto 0)            <= std_logic_vector( stat_rob_hits_ctr );
    stat(16#2E#)(31 downto 0)            <= std_logic_vector( stat_rob_hits2daq_ctr );
    stat(16#2F#)(31 downto 0)            <= std_logic_vector( stat_rob_wren_ctr );
    stat(16#30#)(15 downto 0)            <= std_logic_vector( stat_rob_errs_ctr );
    stat(16#31#)(15 downto 0)            <= std_logic_vector( stat_rob_w32interrupt_ctr );
    stat(16#31#)(31 downto 16)           <= std_logic_vector( stat_rob_inevent_idles_ctr );
    stat(16#32#)(15 downto 0)            <= std_logic_vector( stat_rob_unknown_robword_ctr );
    stat(16#32#)(31 downto 16)           <= std_logic_vector( stat_rob_not_in_event_ctr );
    stat(16#33#)(15 downto 0)            <= std_logic_vector( stat_rob_nonconsecutive_ctr );
    stat(16#33#)(31 downto 16)           <= std_logic_vector( stat_rob_notrailer_ctr );
    stat(16#34#)(15 downto 0)            <= std_logic_vector( stat_rob_badtrailer_evn_ctr );
    stat(16#34#)(31 downto 16)           <= std_logic_vector( stat_rob_badtrailer_wrd_ctr );
    stat(16#35#)(15 downto 0)            <= std_logic_vector( stat_rob_fifo_ofw_losses_ctr );
    stat(16#36#)(31 downto 0)            <= std_logic_vector( stat_rob_fifo_af_ctr(31 downto 0) );
    stat(16#35#)(31 downto 16)           <= std_logic_vector( stat_rob_fifo_af_ctr(47 downto 32) );
    stat(16#37#)(15 downto 0)            <= std_logic_vector( stat_rob_header_bxnbad_ctr );
    stat(16#37#)(31 downto 16)           <= std_logic_vector( stat_rob_toomuchdata_ctr );
    stat(16#38#)(15 downto 0)            <= std_logic_vector( stat_rob_headerlate_ctr );
    stat(16#38#)(31 downto 16)           <= std_logic_vector( stat_rob_headerpurged_ctr );
    stat(16#39#)(31 downto 0)            <= std_logic_vector( stat_rob_trailer2daq_ctr );
    stat(16#3A#)(31 downto 0)            <= std_logic_vector( stat_rob_trailer_read_ctr );
    stat(16#3C#)(31 downto 0)            <= stat_debugmem_port(31 downto 0);
    stat(16#3D#)(31 downto 0)            <= stat_debugmem_port(63 downto 32);
    stat(16#3E#)(31 downto 0)            <= stat_debugmem_port(95 downto 64);
    stat(16#3F#)(31 downto 0)            <= stat_debugmem_port(127 downto 96);

    -- End of automatically-generated VHDL code for register "breakout" signals assignment
    --------------------------------------------------------------------------------



--------------------------------------
--------------------------------------
-- IPbus register
--------------------------------------
--------------------------------------
reg: entity work.ipbus_ctrlreg_v
    generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
    port map(
        clk => clk,
        reset => rst,
        ipbus_in => ipb_in,
        ipbus_out => ipb_out,
        --slv_clk => ttc_clk,
        d => csrd,
        q => csrq,
        --rstb => stat_stb,
        stb => ctrl_stb        );

conf <= csrq;
-- registers that can be frozen with conf_stat_freeze ==> related to the global event building
csrd(STATREG_ROBSTAT_FIRST - 1 downto 0) <= stat(STATREG_ROBSTAT_FIRST - 1 downto 0) when (conf_stat_freeze = '0') 
    else csrd(STATREG_ROBSTAT_FIRST - 1 downto 0);
-- registers that cannot ==> either they are frozen by their own freeze, or they never have to be frozen (debugmem_port)
csrd(N_STAT - 1 downto STATREG_ROBSTAT_FIRST) <= stat(N_STAT - 1 downto STATREG_ROBSTAT_FIRST); -- includes debugmem_port

stat_TTS <= tts;

--------------------------------------
--------------------------------------
-- Event counter, resync management, bunch_ctr
--------------------------------------
--------------------------------------
-- This logic comes from MP7, but the dependence of ec0 on resync and resync on readout_done has been removed

resync  <= '1' when ttc_cmd = TTC_BCMD_RESYNC else '0';
ec0     <= '1' when ttc_cmd = TTC_BCMD_EC0 else '0';

evt_ctr_u   <= eoctr_t(evt_ctr_i);
--evt_ctr     <= evt_ctr_u;

process(ttc_clk)
begin
    if rising_edge(ttc_clk) then
        if ec0 = '1' or ttc_rst = '1' then
            evt_ctr_i <= to_unsigned(1, evt_ctr_i'length); -- CMS rules; first event is 1, not zero
        elsif l1a = '1' then
            evt_ctr_i <= evt_ctr_i + 1;
        end if;
        
        if ttc_rst = '1' or conf_ctr_rst = '1' then -- just counters
            stat_l1a_ctr <= (others => '0');
            stat_resync_ctr <= (others => '0');
        else
            stat_l1a_ctr <= stat_l1a_ctr + bo2int(l1a = '1');
            stat_resync_ctr <= stat_resync_ctr + bo2int(resync = '1');
        end if;
        
        internal_time <= internal_time + 1;
    end if;
 end process;

bcn_incr: entity work.bcn_incr
port map(
    ttc_clk         => ttc_clk,
    bunch_ctr       => bunch_ctr_mp7_in,
    incr            => conf_bcn_incr,
    bunch_ctr_incr  => bunch_ctr,
    orb_ctr_in      => orb_ctr,
    orb_ctr_out     => orb_ctr_u);

-----------------------------------------
-- FIFO for the L1As & Resync
-----------------------------------------
-- * There's no easy way for this memory to get full, because the dt trigger event builder would overflow first
-- and events would start to be very small, thus emptying this fifo faster
l1a_fifo_inst : entity work.qwfifo
generic map (
    DATA_WIDTH => FIFO_WIDTH,
    DEPTH_LOG2 => FIFO_DEPTH)
port map (
    ALMOSTEMPTY => l1ff_AE,
    ALMOSTFULL => l1ff_AF,
    RDCOUNT => l1ff_RDCOUNT,
    WRCOUNT => l1ff_WRCOUNT,
    --RDERR => open,               -- 1-bit output read error
    --WRERR => open,               -- 1-bit output write error
    
    RST => l1ff_RST,                   -- 1-bit input reset
    
    WRCLK => ttc_clk,               -- 1-bit input write clock
    FULL => l1ff_FULL,                 -- 1-bit output full
    WREN => l1ff_WREN,                  -- 1-bit input write enable
    DI => l1ff_DI, -- Input data, width defined by DATA_WIDTH parameter
    
    RDCLK => clk80,               -- 1-bit input read clock
    EMPTY => l1ff_EMPTY,       -- 1-bit output empty
    RDEN => l1ff_RDEN,         -- 1-bit input read enable
    DO => l1ff_DO              -- Output data, width defined by DATA_WIDTH parameter
);

l1ff_DI <= resync & std_logic_vector(internal_time) & orb_ctr_u(RO_ORN_WIDTH-1 downto 0) & evt_ctr_U(RO_EVN_WIDTH-1 downto 0) & bunch_ctr;
-- There is no overflow detection implemented, to in theory an excess of l1as could pass undetected and L1As get lost
-- However, this fifo is 4 times as deep as AMC13's, so long before we should have gone OOS, the AMC13 has
-- Also, we have our own busy threshold, and no more L1As should arrive when we are BSY
l1ff_do_resync <= l1ff_DO(FIFO_WIDTH-1);
l1ff_do_time   <= l1ff_DO(FIFO_WIDTH-1-1 downto FIFO_WIDTH-1-INTERNAL_TIME_WIDTH);
l1ff_do_orn    <= l1ff_DO(RO_EVN_WIDTH + RO_BXN_WIDTH + RO_ORN_WIDTH - 1 downto RO_EVN_WIDTH + RO_BXN_WIDTH);    
l1ff_do_evn    <= l1ff_DO(RO_EVN_WIDTH + RO_BXN_WIDTH - 1 downto RO_BXN_WIDTH);
l1ff_do_bxn    <= l1ff_DO(RO_BXN_WIDTH-1 downto 0);                            

l1ff_WREN <= (l1a or resync) and not l1ff_FULL;
l1ff_RST <= ttc_rst;

stat_l1afifo_rden   <= l1ff_RDEN;
stat_l1afifo_ae     <= l1ff_AE;
stat_l1afifo_full   <= l1ff_FULL;
stat_l1afifo_af     <= l1ff_AF;
stat_l1afifo_wren   <= l1ff_WREN;
stat_l1afifo_empty  <= l1ff_EMPTY;
stat_l1afifo_resync <= l1ff_do_resync;
stat_l1afifo_orn    <= l1ff_do_orn;
stat_l1afifo_evn    <= l1ff_do_evn;
stat_l1afifo_bxn    <= l1ff_do_bxn;




---------------------------------------------------
---------------------------------------------------
-- DT data receiver
---------------------------------------------------
---------------------------------------------------
dt_receiver: entity work.dt_receiver
generic map( N_CHAN => N_CHAN )
port map(
    LVDS_in_p   => LVDS_in_p,
    LVDS_in_n   => LVDS_in_n,
                
    clk_ipb     => clk,
    clk40       => clk40,
    clk120      => clk120,
    clk200      => clk200,
    clk600      => clk600,
    rst40       => rst40,
    
    pd_inertia          => conf_rob_pd_inertia              ,
    pd_decay            => conf_rob_pd_decay                ,
    idelay              => conf_rob_idelay                  ,
    max_inevent_idles   => conf_rob_max_inevent_idles       ,

    debugmem_mux        => conf_rob_debugmem_mux            ,
    debugmem_trig       => conf_rob_debugmem_trig           ,
    debugmem_start      => conf_rob_debugmem_start          ,
    debugmem_advance    => conf_rob_debugmem_advance        ,
    debugmem_port       => stat_debugmem_port               ,
    
    robstats            => robstats,
    conf_ctr_rst        => conf_ctr_rst,
    conf_rob_af_thres   => conf_rob_af_thres,
    
    robff_do            => robff_do     ,
    robff_empty         => robff_empty  ,
    robff_rdclk         => robff_rdclk  ,
    robff_rden          => robff_rden
);

robff_rdclk <= clk80;

SN_RXEN_HR <= (others => '1') when conf_snap12_rxen_use = '0' else conf_snap12_rxen;
SN_SQEN_HR <= (others => '0') when conf_snap12_sqen_use = '0' else conf_snap12_sqen;
SN_ENSD_HR <= (others => '1') when conf_snap12_ensd_use = '0' else conf_snap12_ensd;
stat_snap12_sd <= SN_SD_HR;

--stat_tmp_robff_empty <= robff_empty(31 downto 0);

-- here follows some awful chunk of code to freeze/multiplex the rob non-counter stats
process(clk40)
    variable stat_rob_samples_v_latched     : slv91_v_t(N_CHAN-1 downto 0);
    variable stat_rob_nowlock_v_latched     : std_logic_vector(N_CHAN-1 downto 0);
    variable stat_rob_word12_data_v_latched, stat_rob_word12_weak_v_latched :  slv12_v_t(N_CHAN-1 downto 0);
    variable stat_rob_netedgeshift_v_latched : uint8_v_t(N_CHAN-1 downto 0);
    variable stat_rob_ff_max_occupancy_v_latched : uintFFD_v_t(N_CHAN-1 downto 0);
begin
    if rising_edge(clk40) then
        if conf_stat_freeze = '1' then 
            stat_rob_samples_v_latched      := stat_rob_samples_v_latched  ;
            stat_rob_nowlock_v_latched      := stat_rob_nowlock_v_latched  ;
            stat_rob_word12_data_v_latched  := stat_rob_word12_data_v_latched  ;
            stat_rob_word12_weak_v_latched  := stat_rob_word12_weak_v_latched  ;
            stat_rob_netedgeshift_v_latched := stat_rob_netedgeshift_v_latched  ;
            stat_rob_ff_max_occupancy_v_latched := stat_rob_ff_max_occupancy_v_latched ;
        else
            for ch in N_CHAN-1 downto 0 loop
                stat_rob_samples_v_latched(ch)      := robstats(ch).w12_samples_snapshot  ;
                stat_rob_nowlock_v_latched(ch)      := robstats(ch).w12_lock  ;
                stat_rob_word12_data_v_latched(ch)  := robstats(ch).w12_data_snapshot ;
                stat_rob_word12_weak_v_latched(ch)  := robstats(ch).w12_weak_snapshot ;
                stat_rob_netedgeshift_v_latched(ch) := robstats(ch).netedgeshift  ;
                stat_rob_ff_max_occupancy_v_latched(ch) := robstats(ch).ff_max_occupancy  ;
            end loop;
        end if;
        
        stat_rob_samples_1      <= stat_rob_samples_v_latched(to_integer(conf_rob_stat_mux))(31 downto 0);
        stat_rob_samples_2      <= stat_rob_samples_v_latched(to_integer(conf_rob_stat_mux))(63 downto 32);
        stat_rob_samples_3      <= stat_rob_samples_v_latched(to_integer(conf_rob_stat_mux))(90 downto 64);
        stat_rob_nowlock        <= stat_rob_nowlock_v_latched(to_integer(conf_rob_stat_mux));
        stat_rob_word12_data    <= stat_rob_word12_data_v_latched(to_integer(conf_rob_stat_mux));
        stat_rob_word12_weak    <= stat_rob_word12_weak_v_latched(to_integer(conf_rob_stat_mux));
        stat_rob_netedgeshift   <= stat_rob_netedgeshift_v_latched(to_integer(conf_rob_stat_mux));
        stat_rob_max_ff_occupancy <= stat_rob_ff_max_occupancy_v_latched(to_integer(conf_rob_stat_mux));
    end if;
end process;

-- and the counter stats
mfc_edgeshift:          entity work.muxfreezecount generic map(SIZE => stat_rob_edgeshift_ctr'length        , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_edgeshift_ctr       ,clk=>clk120, ctrs_inc=>rsv2ctrinc(robstats,  0), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_unlock12:           entity work.muxfreezecount generic map(SIZE => stat_rob_unlock12_ctr'length         , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_unlock12_ctr        ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats,  1), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_startstoperr:       entity work.muxfreezecount generic map(SIZE => stat_rob_startstoperr_ctr'length     , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_startstoperr_ctr    ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats,  2), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_parityerr_total:    entity work.muxfreezecount generic map(SIZE => stat_rob_parityerr_total_ctr'length  , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes"  ) port map(output=> stat_rob_parityerr_total_ctr ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats,  3), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_parityerr_nofix:    entity work.muxfreezecount generic map(SIZE => stat_rob_parityerr_nofix_ctr'length  , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes"  ) port map(output=> stat_rob_parityerr_nofix_ctr ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats,  4), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_valid:              entity work.muxfreezecount generic map(SIZE => stat_rob_valid_ctr'length            , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes" ) port map(output=> stat_rob_valid_ctr           ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats,  5), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_w32interrupt:       entity work.muxfreezecount generic map(SIZE => stat_rob_w32interrupt_ctr'length     , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes" ) port map(output=> stat_rob_w32interrupt_ctr    ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats,  6), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_w32valid:           entity work.muxfreezecount generic map(SIZE => stat_rob_w32valid_ctr'length         , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes" ) port map(output=> stat_rob_w32valid_ctr        ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats,  7), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_inevent_idles:      entity work.muxfreezecount generic map(SIZE => stat_rob_inevent_idles_ctr'length    , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes"  ) port map(output=> stat_rob_inevent_idles_ctr   ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats,  8), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_header:             entity work.muxfreezecount generic map(SIZE => stat_rob_header_ctr'length           , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes" ) port map(output=> stat_rob_header_ctr          ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats,  9), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_trailer:            entity work.muxfreezecount generic map(SIZE => stat_rob_trailer_ctr'length          , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes" ) port map(output=> stat_rob_trailer_ctr         ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats, 10), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_hits:               entity work.muxfreezecount generic map(SIZE => stat_rob_hits_ctr'length             , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes" ) port map(output=> stat_rob_hits_ctr            ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats, 11), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_errs:               entity work.muxfreezecount generic map(SIZE => stat_rob_errs_ctr'length             , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes"  ) port map(output=> stat_rob_errs_ctr            ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats, 12), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_unknown_robword:    entity work.muxfreezecount generic map(SIZE => stat_rob_unknown_robword_ctr'length  , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes"  ) port map(output=> stat_rob_unknown_robword_ctr ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats, 13), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_not_in_event:       entity work.muxfreezecount generic map(SIZE => stat_rob_not_in_event_ctr'length     , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes"  ) port map(output=> stat_rob_not_in_event_ctr    ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats, 14), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_nonconsecutive:     entity work.muxfreezecount generic map(SIZE => stat_rob_nonconsecutive_ctr'length   , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_nonconsecutive_ctr  ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats, 15), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_notrailer:          entity work.muxfreezecount generic map(SIZE => stat_rob_notrailer_ctr'length        , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_notrailer_ctr       ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats, 16), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_badtrailer_evn:     entity work.muxfreezecount generic map(SIZE => stat_rob_badtrailer_evn_ctr'length   , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_badtrailer_evn_ctr  ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats, 17), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_badtrailer_wrd:     entity work.muxfreezecount generic map(SIZE => stat_rob_badtrailer_wrd_ctr'length   , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_badtrailer_wrd_ctr  ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats, 18), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_fifo_ofw_losses:    entity work.muxfreezecount generic map(SIZE => stat_rob_fifo_ofw_losses_ctr'length  , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_fifo_ofw_losses_ctr ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats, 19), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_wren:               entity work.muxfreezecount generic map(SIZE => stat_rob_wren_ctr'length             , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes" ) port map(output=> stat_rob_wren_ctr            ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats, 20), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_fifo_af:            entity work.muxfreezecount generic map(SIZE => stat_rob_fifo_af_ctr'length          , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes" ) port map(output=> stat_rob_fifo_af_ctr         ,clk=>clk40,  ctrs_inc=>rsv2ctrinc(robstats, 21), ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );

---------------------------------------------------
---------------------------------------------------
-- DAQlink event formatter and TTS status
---------------------------------------------------
---------------------------------------------------
clk_tts <= clk80;

process(clk80)
    variable l1ff_occupancy : unsigned(FIFO_DEPTH-1 downto 0) := (others => '0');
    -- status management
    type status_t is (idle, second_header, dt_payload, channel_flags, trailer);
    variable status : status_t := idle;
    -- for setting DL's valid, header and trailer
    type wordtype_t is (none, header, trailer, data);
    variable wordtype : wordtype_t := none;
    variable amc13_d_var : std_logic_vector(63 downto 0);
    -- data lengths to report in event
    variable data_length_trailer : unsigned(19 downto 0) := (others => '0');
    -- for management of the resync status
    variable resync_pipe : std_logic := '0';
       variable queued_resyncs : unsigned(7 downto 0) := (others => '0');
--       variable resync_timer : integer range 2**40 - 1 downto 0;
    -- for DT event builder
    variable channel : integer range N_CHAN - 1 downto 0 := 0;
    variable nextch : boolean := false;
    variable evn_interval_keep : unsigned(RO_EVN_WIDTH - 1 downto 0);
    variable ch_started : std_logic_vector(N_CHAN - 1 downto 0);
    variable ch_finished : std_logic_vector(N_CHAN - 1 downto 0);
    variable robff_do_wastrailer : std_logic_vector(N_CHAN - 1 downto 0);
    variable ch_wordctr : uint8_v_t(N_CHAN - 1 downto 0);
    variable semiword : integer range 1 downto 0;
    variable ch_errors  : slv12_v_t(N_CHAN - 1 downto 0);
    variable dummydata_div_ctr : unsigned(data_length_trailer'range) := (others => '0');
    variable dummydata_mul_ctr : unsigned(conf_dteb_dummydata_mul'range) := (others => '0');
begin
    if rising_edge(clk80) then
        if rst40 = '1' then
            l1ff_rden <= '0';
            
            status := idle;
            amc13_resync <= '0';
            amc13_d <= (others => '0');
            amc13_valid     <= '0';
            amc13_hdr     <= '0';
            amc13_trl <= '0';
            amc13_resync <= '0';
            resync_pipe := '0';
            queued_resyncs := (others => '0');
            tts <= x"8";

            stat_evb_fsm_state          <= (others => '0');
            stat_resync_queued          <= (others => '0');
            stat_resync_queued_max      <= (others => '0'); 
            stat_header_ctr             <= (others => '0');
            stat_trailer_ctr            <= (others => '0');
            stat_valid_ctr              <= (others => '0');
            stat_tts_rdy_ctr            <= (others => '0');
            stat_tts_ofw_ctr            <= (others => '0');
            stat_tts_bsy_ctr            <= (others => '0');
            stat_l1afifo_occupancy      <= (others => '0');
            stat_l1afifo_occupancy_max  <= (others => '0');
            stat_max_evt_size_ctr       <= (others => '0');
            stat_big_evt_size_ctr       <= (others => '0');
            
            robff_do_wastrailer := (others => '0');
        else
            wordtype := none;
            l1ff_rden <= '0';
            robff_rden <= (others => '0');
            
            ctrinc_header_bxnbad <= (others => '0');
            ctrinc_toomuchdata   <= (others => '0');
            ctrinc_headerlate    <= (others => '0');
            ctrinc_headerpurged  <= (others => '0');
            ctrinc_hits2daq      <= (others => '0');
            ctrinc_trailer2daq   <= (others => '0');
            ctrinc_trailer_read  <= (others => '0');
            
            stat_evb_fsm_state <= x"0";
            if amc13_rdy = '1' and amc13_af = '0' and  l1ff_do_resync = '0' then
                case status is
                    when idle =>
                        stat_evb_fsm_state <= x"1";
                        if l1ff_empty = '0' then
                            stat_evb_fsm_state <= x"2";
                            wordtype := header;
                            amc13_d <= x"00" & l1ff_do_evn(23 downto 0) & l1ff_do_bxn & x"FFFFF";-- length unknown a priori
                            status := second_header;
                            data_length_trailer := (others => '0');
                        end if;
                    when second_header =>
                        stat_evb_fsm_state <= x"3";
                        wordtype := data;
                        amc13_d <= conf_userData(27 downto 0) & tts & l1ff_do_orn & conf_boardID;
                        status := dt_payload;
                        channel := 0;
                        nextch := false;
                        ch_started := conf_dteb_disable_ch;
                        ch_finished := conf_dteb_disable_ch;
                        ch_wordctr := (others => (others => '0'));
                        ch_errors := (others => (others => '0'));
                        semiword := 1;
                        stat_tmp_dt_time <= (others => '0');
                        stat_tmp_purge_last <= '0';
                        dummydata_mul_ctr := (others => '0');
                        dummydata_div_ctr := (others => '0');
                    when dt_payload =>
                        stat_tmp_dt_time <= stat_tmp_dt_time + 1;
                        stat_evb_fsm_state <= x"4";
                        amc13_d(63 downto 61) <= "001";
                        amc13_d(31 downto 29) <= "000";

                        -- Sequential action on every channel
                        if ch_finished /= (ch_finished'range => '1') then
                            stat_evb_fsm_state(0) <= '1';
                            if ch_started(channel) = '1' and ch_finished(channel) = '0' and robff_empty(channel) = '0' then
                                if robff_do(channel)(31 downto 28) = x"0" or robff_do(channel)(31 downto 28) = x"1" or robff_do(channel)(31 downto 28) = x"F" then
                                    robff_rden(channel) <= bo2sl( (robff_do(channel)(31 downto 28) = x"1") or (robff_do(channel)(31 downto 28) = x"F") );
                                    ch_finished(channel) := '1';
                                    nextch := true;
                                    if robff_do(channel)(31 downto 28) = x"F" then ch_errors(channel)(6 downto 0) := robff_do(channel)(6 downto 0);
                                    elsif robff_do(channel)(31 downto 28) = x"0" then ch_errors(channel)(7) := '1';
                                    else ctrinc_trailer2daq(channel) <= '1';
                                    end if;
                                elsif ch_wordctr(channel) =  conf_dteb_max_words_per_ch then
                                    ch_finished(channel) := '1';
                                    nextch := true;
                                    ch_errors(channel)(8) := '1';
                                    ctrinc_toomuchdata(channel) <= '1';
                                elsif robff_do(channel)(31 downto 28) = x"4" or robff_do(channel)(31 downto 28) = x"6" then
                                    ch_wordctr(channel) := ch_wordctr(channel) + 1;
                                    robff_rden(channel) <= '1';
                                    amc13_d( semiword*32 + 28 downto semiword*32 + 19 ) <= robff_do(channel)(29) & std_logic_vector(to_unsigned(channel,7)) & robff_do(channel)(25 downto 24);
                                    if robff_do(channel)(31 downto 28) = x"4" then  amc13_d( semiword*32 + 18 downto semiword*32 ) <= robff_do(channel)(23 downto 19) & robff_do(channel)(15 downto 2);
                                    else                                            amc13_d( semiword*32 + 18 downto semiword*32 ) <= robff_do(channel)(18 downto 0);
                                    end if;
                                    if semiword = 0 then wordtype := data;
                                    else dummydata_div_ctr := dummydata_div_ctr + 1;
                                    end if;
                                    semiword := 1 - semiword ;
                                    ctrinc_hits2daq(channel) <= bo2sl( robff_do(channel)(31 downto 28) = x"4" );
                                end if;
                            else 
                                nextch := true;
                            end if;
                        elsif semiword = 0 then
                            amc13_d( 28 downto 0 ) <= '1' & x"FFFFFFF"; 
                            wordtype := data;
                            semiword := 1 - semiword ;
                        elsif (dummydata_div_ctr > 0 or dummydata_mul_ctr > 0) and conf_dteb_dummydata_mul > 0 then
                            amc13_d( 28 downto 0 ) <= '1' & x"FFFFFFF";
                            amc13_d( 60 downto 32 ) <= '1' & x"FFFFFFF";
                            wordtype := data;

                            if dummydata_mul_ctr > 0 then 
                                dummydata_mul_ctr := dummydata_mul_ctr - 1;
                            elsif dummydata_div_ctr >= 2**DUMMYDATA_LOG2_DIV then
                                dummydata_div_ctr := dummydata_div_ctr - 2**DUMMYDATA_LOG2_DIV;
                                dummydata_mul_ctr := conf_dteb_dummydata_mul;
                            elsif dummydata_div_ctr > 0 then
                                dummydata_div_ctr := dummydata_div_ctr - 1;
                                dummydata_mul_ctr(dummydata_mul_ctr'high - DUMMYDATA_LOG2_DIV downto 0) := conf_dteb_dummydata_mul(conf_dteb_dummydata_mul'high downto DUMMYDATA_LOG2_DIV); 
                            end if;
                            
                        else
                            status := channel_flags ;
                            channel := 0; -- "sub-status" for status = channel_flags
                        end if;
                        
                        if      nextch and channel = N_CHAN - 1 then    channel := 0;
                        elsif   nextch                          then    channel := channel + 1;
                        end if;
                        nextch := false;
                    
                        evn_interval_keep := unsigned(evt_ctr_U(RO_EVN_WIDTH-1 downto 0)) - unsigned(l1ff_do_evn);
                        stat_tmp_timesupdiff <= internal_time - unsigned(l1ff_do_time);
                        stat_evb_fsm_state(1) <= bo2sl(internal_time - unsigned(l1ff_do_time) > conf_dteb_max_hdr_delay); --timesup
                        -- Parallel action on all channels: 
                        for ch in N_CHAN - 1 downto 0 loop
                            -- process header, and purge invalid words
                            if (ch_started(ch) = '0' or ch_finished(ch) = '1') and robff_empty(ch) = '0' then --ch_finished condition is to purge discarded data
                                if robff_do(ch)(31 downto 24) = x"0F"
                                   and unsigned(robff_do(ch)(23 downto 12)) + bo2int(conf_dteb_rob_evn_inc = '1') = unsigned(l1ff_do_evn(11 downto 0)) then
                                    robff_rden(ch) <= '1';
                                    if conf_dteb_enforce_bxn = '0' or robff_do(ch)(11 downto 0) = l1ff_do_bxn(11 downto 0) then 
                                        ch_started(ch) := '1'; 
                                    end if; 
                                    ctrinc_header_bxnbad(ch) <= bo2sl( robff_do(ch)(11 downto 0) /= l1ff_do_bxn(11 downto 0) );
                                elsif not ( robff_do(ch)(31 downto 24) = x"0F" 
                                  and unsigned(robff_do(ch)(23 downto 12)) + bo2int(conf_dteb_rob_evn_inc = '1') - unsigned(l1ff_do_evn(11 downto 0)) < evn_interval_keep )  then
                                    robff_rden(ch) <= '1';
                                    ctrinc_headerpurged(ch) <= bo2sl( robff_do(ch)(31 downto 24) = x"0F" );
                                    if robff_do(ch)(31 downto 24) = x"0F" and ch = 24 then
                                        stat_tmp_purge_time <=  stat_tmp_dt_time;
                                        stat_tmp_purge_evn <= robff_do(ch)(23 downto 12);
                                        stat_tmp_purge_last <= '1';
                                    end if;
                                end if;
                            end if;
                            -- set all non-started channels to finished when time threshold is crossed
                            -- if event processing starts, due to a high backpressure, after the timeout expires,
                            -- all the channels that are behaving well will have the header prepared so it will be started
                            -- before the timeout discarding is executed.
                            -- Unclean channels (for example channels left at the middle because of a word_max will however not
                            -- be able to clean before timeout fires.
                            if (internal_time - unsigned(l1ff_do_time) > conf_dteb_max_hdr_delay) and ch_started(ch) = '0' and ch_finished(ch) = '0' then -- ch_finished check to prevent double counting
                                ch_finished(ch) := '1'; 
                                ch_errors(ch)(9) := '1';
                                ctrinc_headerlate(ch) <= '1';
                            end if;
                        end loop;
                        -- set all channels to finished when size threshold is crossed 
                        if (conf_dteb_max_words_per_ev /= (conf_dteb_max_words_per_ev'range => '0')) and 
                           (data_length_trailer(conf_dteb_max_words_per_ev'range) = conf_dteb_max_words_per_ev) then
                            status := channel_flags ;
                            channel := 0; -- "sub-status" for status = channel_flags
                            for ch in N_CHAN - 1 downto 0 loop
                                ch_errors(ch)(10) := not ch_finished(ch); -- error inside payload same for unstarted and unfinished
                            end loop;
                            stat_max_evt_size_ctr <= stat_max_evt_size_ctr + 1;
                        end if;

                    when channel_flags =>
                        --CAREFUL!!! variable channel here refers to the MTP, being the channel inside this mtp = 12*channel + mtpch
                        stat_evb_fsm_state <= x"8";
                        if (conf_dteb_status_mask = (conf_dteb_status_mask'range => '0')) then
                            status := trailer;
                        else
                            amc13_d_var(63 downto 60) := std_logic_vector(to_unsigned(8 + channel,4));
                            for mtpch in 11 downto 0 loop
                                amc13_d_var(5*(mtpch+1)-1 downto 5*mtpch+1) := "0000"; 
                                amc13_d_var(5*mtpch) := bo2sl( (ch_errors(12*channel + mtpch) and conf_dteb_status_mask) /= (conf_dteb_status_mask'range => '0') );
                            end loop;
                            amc13_d <= amc13_d_var;
                            if (amc13_d_var(59 downto 0) /= (59 downto 0 => '0')) then
                                wordtype := data;
                            end if;
                            channel := channel + 1;
                            if channel = 6 then status := trailer;
                            end if;
                        end if;

                    when others => --trailer
                        stat_evb_fsm_state <= x"F";
                        wordtype := trailer;
                        amc13_d <= x"00000000" & l1ff_do_evn(7 downto 0) & x"0" & std_logic_vector(data_length_trailer + 1);
                        status := idle;
                        l1ff_rden <= '1';
                end case;
            end if;

            for ch in N_CHAN - 1 downto 0 loop
                -- this detects when a trailer has been read from each robff
                ctrinc_trailer_read(ch) <= robff_rden(ch) and robff_do_wastrailer(ch);
                robff_do_wastrailer(ch) := bo2sl(robff_empty(ch) = '0' and robff_do(ch)(31 downto 28) = x"1");
                -- this keeps pruning unuseful data from fifos even out of dt_payload
                if status /= dt_payload and robff_empty(ch) = '0' and robff_do(ch)(31 downto 24) /= x"0F" then
                    robff_rden(ch) <= '1'; -- do not change to bo2sl because it overwrites rden asserted before
                end if;
            end loop;
            
            amc13_valid <= bo2sl(wordtype /= none);
            amc13_hdr   <= bo2sl(wordtype = header);
            amc13_trl   <= bo2sl(wordtype = trailer);
            if wordtype /= none then data_length_trailer := data_length_trailer + 1;
            end if;

            -- when BGo = resync, increase the pending resyncs (pipe to account for different clocks)
            if resync_pipe = '0' and resync = '1' then 
                queued_resyncs := queued_resyncs + 1;
            end if;
            resync_pipe := resync;
            -- Processing a ReSync:
            if l1ff_do_resync = '1' and l1ff_empty = '0' then 
                queued_resyncs := queued_resyncs - 1;
                l1ff_rden <= '1';
            end if;
            amc13_resync <= l1ff_do_resync and not l1ff_empty;
            
            -- to-do:
            -- if queued_resyncs >= 2 or resync_timer = 0 or l1ff_occupancy >= threshold
            -- you should force the resync: reset all memories, then assert amc13_resync
            
            l1ff_occupancy := unsigned(l1ff_WRCOUNT) - unsigned(l1ff_RDCOUNT);
            --TTS: 8-ready, 4-busy, 2-oos, 1-warning_overflow, C-error, F-disconnected
            case tts is
                when x"4" =>
                    if l1ff_do_resync = '1' and queued_resyncs = 0 then
                        tts <= x"8";
                    elsif l1ff_occupancy < conf_tts_thresholds_bsy2ofw then
                        tts <= x"1";
                    end if;
                when x"1" =>
                    if l1ff_occupancy < conf_tts_thresholds_ofw2rdy then
                        tts <= x"8";
                    elsif l1ff_occupancy > conf_tts_thresholds_ofw2bsy then
                        tts <= x"4";
                    end if;
                when x"8" =>
                    if l1ff_occupancy > conf_tts_thresholds_rdy2ofw then
                        tts <= x"1";
                    end if;
                when others =>
                    tts <= x"8";
            end case;
            
            if queued_resyncs > 0 then tts <= x"4";
            end if;
            
            stat_resync_queued <= queued_resyncs;
            stat_l1afifo_occupancy <= l1ff_occupancy;
            if conf_ctr_rst = '1' then
                stat_resync_queued_max      <= (others => '0');
                stat_header_ctr             <= (others => '0');
                stat_trailer_ctr            <= (others => '0');
                stat_valid_ctr              <= (others => '0');
                stat_tts_rdy_ctr            <= (others => '0');
                stat_tts_ofw_ctr            <= (others => '0');
                stat_tts_bsy_ctr            <= (others => '0');
                stat_l1afifo_occupancy_max  <= (others => '0');
                stat_max_evt_size_ctr       <= (others => '0');
                stat_big_evt_size_ctr       <= (others => '0');
            else
                stat_resync_queued_max      <= maximum( stat_resync_queued_max, queued_resyncs );
                stat_header_ctr             <= stat_header_ctr + bo2int(amc13_hdr = '1' and amc13_valid = '1');
                stat_trailer_ctr            <= stat_trailer_ctr + bo2int(amc13_trl = '1' and amc13_valid = '1');
                stat_valid_ctr              <= stat_valid_ctr + bo2int(amc13_valid = '1');
                stat_tts_rdy_ctr            <= stat_tts_rdy_ctr + bo2int(tts = x"8");
                stat_tts_ofw_ctr            <= stat_tts_ofw_ctr + bo2int(tts = x"1");
                stat_tts_bsy_ctr            <= stat_tts_bsy_ctr + bo2int(tts = x"4");
                stat_l1afifo_occupancy_max  <= maximum( stat_l1afifo_occupancy_max, stat_l1afifo_occupancy );
                stat_big_evt_size_ctr       <= stat_big_evt_size_ctr + bo2int(amc13_valid = '1' and data_length_trailer(conf_dteb_big_evt_size_ctr_threshold'range) = conf_dteb_big_evt_size_ctr_threshold);
            end if;
        end if;
    end if;
end process;

mfc_ctrinc_header_bxnbad: entity work.muxfreezecount generic map(SIZE => stat_rob_header_bxnbad_ctr'length , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_header_bxnbad_ctr ,clk=>clk80, ctrs_inc=>ctrinc_header_bxnbad , ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_ctrinc_toomuchdata:   entity work.muxfreezecount generic map(SIZE => stat_rob_toomuchdata_ctr'length   , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_toomuchdata_ctr   ,clk=>clk80, ctrs_inc=>ctrinc_toomuchdata   , ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_ctrinc_headerlate:    entity work.muxfreezecount generic map(SIZE => stat_rob_headerlate_ctr'length    , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_headerlate_ctr    ,clk=>clk80, ctrs_inc=>ctrinc_headerlate    , ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_ctrinc_headerpurged:  entity work.muxfreezecount generic map(SIZE => stat_rob_headerpurged_ctr'length  , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_headerpurged_ctr  ,clk=>clk80, ctrs_inc=>ctrinc_headerpurged  , ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_ctrinc_hits2daq:      entity work.muxfreezecount generic map(SIZE => stat_rob_hits2daq_ctr'length      , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"yes" ) port map(output=> stat_rob_hits2daq_ctr      ,clk=>clk80, ctrs_inc=>ctrinc_hits2daq      , ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_ctrinc_trailer2daq:   entity work.muxfreezecount generic map(SIZE => stat_rob_trailer2daq_ctr'length   , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_trailer2daq_ctr   ,clk=>clk80, ctrs_inc=>ctrinc_trailer2daq   , ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );
mfc_ctrinc_trailer_read:  entity work.muxfreezecount generic map(SIZE => stat_rob_trailer_read_ctr'length  , N_CTRS=>N_CHAN, MUX_SIZE=>7, USE_DSP=>"no"  ) port map(output=> stat_rob_trailer_read_ctr  ,clk=>clk80, ctrs_inc=>ctrinc_trailer_read  , ctr_ena=>conf_rob_ctr_ena, freeze=>conf_stat_freeze, mux=>conf_rob_stat_mux, rst=>conf_ctr_rst );

--------------------------------------
--------------------------------------
-- AMC13 interface
--------------------------------------
--------------------------------------

process (clk)
    variable counter : integer range 2**15-1 downto 0;
    variable rstfire_pipe : std_logic := '0';
begin
    if rising_edge(clk) then
        if rst = '1' then
            rst_link <= '0';
            rstfire_pipe := '0';
            counter := 0;

            stat_amc13_reset_ctr    <= (others => '0');
            stat_amc13_rdy_ctr      <= (others => '0');
            stat_amc13_nrdy_ctr     <= (others => '0');
            stat_amc13_af_ctr       <= (others => '0');
        else
            if rstfire_pipe /= conf_amc13_rst_edge then    counter := 2**(to_integer(conf_amc13_rst_len))-1;  
            elsif counter > 0 then              counter := counter - 1;
            end if;
            rstfire_pipe := conf_amc13_rst_edge;
            
            rst_link <= bo2sl(counter > 0) or conf_amc13_rst_hold;
            
            if conf_ctr_rst = '1' then
                stat_amc13_reset_ctr    <= (others => '0');
                stat_amc13_rdy_ctr      <= (others => '0');
                stat_amc13_nrdy_ctr     <= (others => '0');
                stat_amc13_af_ctr       <= (others => '0');
            else
                stat_amc13_reset_ctr    <= stat_amc13_reset_ctr    + bo2int(rst_link = '1');
                stat_amc13_rdy_ctr      <= stat_amc13_rdy_ctr      + bo2int(amc13_rdy = '1');
                stat_amc13_nrdy_ctr     <= stat_amc13_nrdy_ctr     + bo2int(amc13_rdy = '0');
                stat_amc13_af_ctr       <= stat_amc13_af_ctr       + bo2int(amc13_af = '1');
            end if;
        end if;
    end if;
end process;

-- My amc13link... Original MP7's is in amc13_link.vhd
amc13: entity work.tm7_amc13link
port map(
    sysclk            => clk            ,
    reset             => rst_link       ,  -- asynchronous reset, assert reset until GTX REFCLK stable
    -- MGT ports
    AMC_P1_RX_P       => AMC_P1_RX_P    ,
    AMC_P1_RX_N       => AMC_P1_RX_N    ,
    AMC_P1_TX_N       => AMC_P1_TX_N    ,
    AMC_P1_TX_P       => AMC_P1_TX_P    ,
    amc13_refclk      => amc13_refclk   ,
    --MGT_CLK_115_1_P        => MGT_CLK_115_1_P    ,
    --MGT_CLK_115_1_N        => MGT_CLK_115_1_N    ,
    -- TTS port
    TTSclk            => clk_tts        ,  -- clock source which clocks TTS signals
    TTS               => tts            ,
    -- Data port
    ReSyncAndEmpty    => amc13_resync   ,-- asserted for two clk80 cycles, because amc13_rsae has a >10 ns requirement
    EventDataClk      => clk80          ,
    EventData_valid   => amc13_valid    , -- used as data write enable
    EventData_header  => amc13_hdr      , -- first data word
    EventData_trailer => amc13_trl      , -- last data word
    EventData         => amc13_d        ,
    AlmostFull        => amc13_af       , -- buffer almost full
    Ready             => amc13_rdy      ,
    debug             => stat_gth
    );

stat_amc13_rdy <= amc13_rdy;
stat_amc13_af <= amc13_af;

end Behavioral;
