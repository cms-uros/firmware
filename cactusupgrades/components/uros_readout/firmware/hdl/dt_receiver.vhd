----------------------------------------------------------------------------------
-- Receive ROB data and aligns received events
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
--use work.uROS_data_types.all;
use work.uros_readout_decl.all;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.minmax.all;

entity dt_receiver is
    Generic ( 
        N_CHAN      : natural := 72;
        WID_INERTIA : natural := 3;
        WID_DECAY   : natural := 8;
        WID_IDELAY  : natural := 5
        );
    Port (
        LVDS_in_p   : in std_logic_vector(71 downto 0);
        LVDS_in_n   : in std_logic_vector(71 downto 0);
        
        clk_ipb     : in std_logic;
        clk40       : in std_logic;
        clk120      : in std_logic;
        clk200      : in std_logic;
        clk600      : in std_logic;
        rst40       : in std_logic;
        
        pd_inertia          : in std_logic_vector(N_CHAN*WID_INERTIA-1 downto 0);
        pd_decay            : in std_logic_vector(N_CHAN*WID_DECAY-1   downto 0);
        idelay              : in std_logic_vector(N_CHAN*WID_IDELAY-1  downto 0);
        
        max_inevent_idles   : in unsigned(5 downto 0);
        
        debugmem_mux        : in unsigned(6 downto 0);
        debugmem_trig       : in unsigned(4 downto 0);
        debugmem_start      : in std_logic;
        debugmem_advance    : in std_logic;
        debugmem_port       : out std_logic_vector(127 downto 0);
        
        robstats            : out rrstats_v_t ;
        conf_ctr_rst        : in std_logic;
        conf_rob_af_thres   : in unsigned(ROBFIFO_DEPTH-1 downto 0);
        
        robff_do    : out ipb_reg_v(N_CHAN-1 downto 0);
        robff_empty : out std_logic_vector(N_CHAN-1 downto 0);
        robff_rdclk : in std_logic;
        robff_rden  : in std_logic_vector(N_CHAN-1 downto 0)
        );
end dt_receiver;

architecture Behavioral of dt_receiver is
    signal robs_stats : rrstats_v_t(N_CHAN-1 downto 0);
    
    attribute IODELAY_GROUP : STRING;
    attribute IODELAY_GROUP of idelayctrl_inst: label is "lvds_in";

begin

robstats <= robs_stats;

idelayctrl_inst : IDELAYCTRL
   port map (
      RDY => open,       -- 1-bit output: Ready output
      REFCLK => clk200, -- 1-bit input: Reference clock input
      RST => rst40        -- 1-bit input: Active high reset input
   );


---------------------------------------------------
-- Per-channel deserializers
---------------------------------------------------
gen1: for i in N_CHAN-1 downto 0 generate
    rr : entity work.rob_receiver
    port map(
        LVDS_in_p => LVDS_in_p(i),
        LVDS_in_n => LVDS_in_n(i),
        clk40 => clk40,
        clk120 => clk120,
        clk600 => clk600,
        rst40 => rst40,
        
        pd_inertia => to_integer(unsigned( pd_inertia((i+1)*(WID_INERTIA)-1 downto i*(WID_INERTIA) ))),
        pd_decay   => to_integer(unsigned( pd_decay((i+1)*(WID_DECAY)-1   downto i*(WID_DECAY)   ))),
        idelay     => idelay((i+1)*(WID_IDELAY)-1  downto i*(WID_IDELAY)  ),
        
        max_inevent_idles => max_inevent_idles,
        
        robff_rdclk     => robff_rdclk,     
        robff_empty     => robff_empty(i),  
        robff_rden      => robff_rden(i),   
        robff_do        => robff_do(i),      
       
        conf_ctr_rst        => conf_ctr_rst,
        conf_rob_af_thres   => conf_rob_af_thres,
        
        stats        => robs_stats(i)
        );

end generate;



---- this exists only during development, to reduce the implementation time
--gen2: for i in 71 downto N_CHAN generate
--    isd : entity work.isampler
--        PORT MAP (
--            lvds_in_p => LVDS_in_p(i),
--            lvds_in_n => LVDS_in_n(i),
--            idelaycnt => "00000",
--            iserdes_q => open,
--            clk_in => clk600,
--            clk_div_in => clk120,
--            io_reset => rst40);
--end generate;

---------------------------------------------------
-- spy mem to debug rob_receiver behaviour
---------------------------------------------------

gen4: if true generate
    signal running : std_logic;
    signal mem_wadd, mem_radd : std_logic_vector(9 downto 0);
    signal mem_wen : std_logic;
    signal mem_din : std_logic_vector(127 downto 0);

begin

    debugmem : entity work.multi_bram_sdp 
    GENERIC MAP(
        DATA_WIDTH => 128,
        DEPTH_LOG2 => 10, 
        BR18_ADDR_WIDTH => 10) 
    PORT MAP(
        wclk => clk120,
        wen => mem_wen,
        wadd => mem_wadd,
        din => mem_din,
        
        rclk => clk_ipb,
        ren => '1',
        radd => mem_radd,
        dout => debugmem_port );
        
        
    process(clk120)
        variable advance_delay_goal : std_logic;
        variable advance_delay : unsigned(3 downto 0);
        variable word12_toggle_old : std_logic;
        variable postrecord : unsigned(7 downto 0);
        variable do_wen : std_logic;
        variable forever_run : unsigned(3 downto 0);
    begin
        if rising_edge(clk120) then
            forever_run := forever_run + 1;
            
            mem_din(11 downto  0) <= robs_stats(to_integer(debugmem_mux)).w12_data;
            mem_din(23 downto 12) <= robs_stats(to_integer(debugmem_mux)).w12_weak;
            mem_din(24)           <= robs_stats(to_integer(debugmem_mux)).w12_lock;
            mem_din(25)           <= robs_stats(to_integer(debugmem_mux)).w12_toggle;
            mem_din(26)           <= (robs_stats(to_integer(debugmem_mux)).w12_toggle xor word12_toggle_old);
            mem_din(30 downto 27) <= std_logic_vector(forever_run);
            
            mem_din(122 downto 32)<= robs_stats(to_integer(debugmem_mux)).w12_samples;
            
            do_wen := running; --and (word12_toggle(to_integer(debugmem_mux)) xor word12_toggle_old);
            mem_wen <= do_wen;
            mem_wadd <= std_logic_vector(unsigned(mem_wadd) + bo2int(do_wen = '1'));
            word12_toggle_old := robs_stats(to_integer(debugmem_mux)).w12_toggle;
            
            if postrecord = (postrecord'range => '0') then
                running <= '0';
            elsif postrecord = (postrecord'range => '1') then
                if robs_stats(to_integer(debugmem_mux)).counters_inc(to_integer(debugmem_trig)) = '1' then
                    postrecord := postrecord - 1;
                    mem_din(31) <= '0';
                end if;
            else postrecord := postrecord - 1;
            end if;
            
            if debugmem_start = '1' then
                running <= '1';
                postrecord := (others => '1');
                mem_din(31)<= '1';
            end if;

            if running = '1' then
                mem_radd <= std_logic_vector(unsigned(mem_wadd) - 1);
            elsif advance_delay = (advance_delay'range => advance_delay_goal) then
                mem_radd <= std_logic_vector(unsigned(mem_radd) - 1);
                advance_delay_goal := not advance_delay_goal;
            end if;
            if advance_delay /= (advance_delay'range => '1') and debugmem_advance = '1' then advance_delay := advance_delay + 1;
            elsif advance_delay /= (advance_delay'range => '0') and debugmem_advance = '0' then advance_delay := advance_delay - 1;
            end if;
        end if;
    end process;

end generate;

end Behavioral;
