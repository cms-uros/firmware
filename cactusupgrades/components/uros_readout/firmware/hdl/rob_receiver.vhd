----------------------------------------------------------------------------------
-- Instantiates ISERDES, aligns to bit edges, and creates 12-bit input word
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
--use work.uROS_data_types.all;
use work.uros_readout_decl.all;
use work.ipbus_reg_types.all;
use work.minmax.all;

entity rob_receiver is
    Generic (
        pd_bits : natural := 8 --cuantos bits tiene el acumulador de cambio posici�n de las transiciones (edge)
        -- 2^(pd_bits-1) es el n�mero de transiciones en el mismo sentido que tiene que haber para que cambie
        -- si hay al menos 2 transiciones de cada 12 bits, cuando se cambia se cambia 1/4 de bit, y es esperable
        -- que a frecuencia cambie un 1/N (se salte un ciclo de cada N), se tiene que cumplir que
        -- pd_bits < log2(4*N/3). para un 1/1000, pd_bits < 10. Cumpliendo esto, cuanto m�s peque�o, m�s variar�,
        -- cuanto m�s grande, m�s estabe ser�. De todas formas, no es malo que cambie mucho, porque para pasar de edge
        -- a punto de muestreo est� nuevamente estabilizado y optimizado para que cambie en el momento ideal.
        -- Sin embargo, si es m�s peque�o, deber�a tener una respuesta m�s r�pida para el caso de que haya cambios bruscos
        );
    Port (
        LVDS_in_p : in std_logic;
        LVDS_in_n : in std_logic;
        clk40 : in std_logic;
        clk120 : in std_logic;
        clk600 : in std_logic;
        rst40 : in std_logic;
        
        pd_inertia  : in integer range pd_bits-1 downto 0;
        pd_decay    : in integer range 255 downto 0;
        idelay      : in std_logic_vector(4 downto 0);
        
        max_inevent_idles : in unsigned(5 downto 0);

        robff_rdclk     : in std_logic;
        robff_empty     : out std_logic;
        robff_rden      : in std_logic;
        robff_do        : out std_logic_vector(31 downto 0);

        conf_ctr_rst    : in std_logic;
        conf_rob_af_thres: in unsigned(ROBFIFO_DEPTH-1 downto 0);
        stats           : out rrstats_t := rrstats_init
        );
        
end rob_receiver;

architecture Behavioral of rob_receiver is
    signal iserdes_q : std_logic_vector(9 downto 0);

    signal word12_data     : std_logic_vector(11 downto 0);
    signal word12_weak     : std_logic_vector(11 downto 0);
    signal word12_toggle   : std_logic;
    signal word12_lock     : std_logic;
    signal samples         : std_logic_vector(90 downto 0);
    
    signal robff_full       : std_logic;
    signal robff_wren       : std_logic;
    signal robff_di         : std_logic_vector(31 downto 0);
    signal rdctr, wrctr     : std_logic_vector(ROBFIFO_DEPTH-1 downto 0);

    constant ERROR_WORD_WORD32_UNLOCK   : std_logic_vector := x"F0000001";
    constant ERROR_WORD_OUTSIDE_EVENT   : std_logic_vector := x"F0000002";
    constant ERROR_WORD_UNKOWN_ROBWORD  : std_logic_vector := x"F0000004";
    constant ERROR_WORD_TRAILER_EVN_BAD : std_logic_vector := x"F0000008";
    constant ERROR_WORD_TRAILER_CNT_BAD : std_logic_vector := x"F0000010";
    constant ERROR_WORD_FIFO_OVERFLOW   : std_logic_vector := x"F0000020";
    constant ERROR_INEVENT_IDLES        : std_logic_vector := x"F0000040";

    -- counters increase signals
    -- clk120
    signal ctrinc_edgeshift       : std_logic;
    -- clk40        
    signal ctrinc_unlock12        : std_logic;
    signal ctrinc_startstoperr    : std_logic;
    signal ctrinc_parityerr_total : std_logic;
    signal ctrinc_parityerr_nofix : std_logic;
    signal ctrinc_valid           : std_logic;
    signal ctrinc_w32interrupt    : std_logic;
    signal ctrinc_w32valid        : std_logic;
    signal ctrinc_inevent_idles   : std_logic;
    signal ctrinc_header          : std_logic;
    signal ctrinc_trailer         : std_logic;
    signal ctrinc_hits            : std_logic;
    signal ctrinc_errs            : std_logic;
    signal ctrinc_unknown_robword : std_logic;
    signal ctrinc_not_in_event    : std_logic;
    signal ctrinc_nonconsecutive  : std_logic;
    signal ctrinc_notrailer       : std_logic;
    signal ctrinc_badtrailer_evn  : std_logic;
    signal ctrinc_badtrailer_wrd  : std_logic;
    signal ctrinc_fifo_ofw_losses : std_logic;
    signal ctrinc_wren            : std_logic;
    signal ctrinc_fifo_af         : std_logic;

    function majority5 (bitsin: std_logic_vector(4 downto 0)) return std_logic is
    begin
      case bitsin is
        when "00000" => return '0';
        when "00001" => return '0';
        when "00010" => return '0';
        when "00011" => return '0';
        when "00100" => return '0';
        when "00101" => return '0';
        when "00110" => return '0';
        when "00111" => return '1';
        when "01000" => return '0';
        when "01001" => return '0';
        when "01010" => return '0';
        when "01011" => return '1';
        when "01100" => return '0';
        when "01101" => return '1';
        when "01110" => return '1';
        when "01111" => return '1';
        when "10000" => return '0';
        when "10001" => return '0';
        when "10010" => return '0';
        when "10011" => return '1';
        when "10100" => return '0';
        when "10101" => return '1';
        when "10110" => return '1';
        when "10111" => return '1';
        when "11000" => return '0';
        when "11001" => return '1';
        when "11010" => return '1';
        when "11011" => return '1';
        when "11100" => return '1';
        when "11101" => return '1';
        when "11110" => return '1';
        when "11111" => return '1';
      end case;
    end function majority5;
    
    function weak5 (bitsin: std_logic_vector(4 downto 0)) return std_logic is
    begin
      case bitsin is
        when "00000" => return '0';
        when "00001" => return '0';
        when "00010" => return '0';
        when "00011" => return '1';
        when "00100" => return '0';
        when "00101" => return '1';
        when "00110" => return '1';
        when "00111" => return '1';
        when "01000" => return '0';
        when "01001" => return '1';
        when "01010" => return '1';
        when "01011" => return '1';
        when "01100" => return '1';
        when "01101" => return '1';
        when "01110" => return '1';
        when "01111" => return '0';
        when "10000" => return '0';
        when "10001" => return '1';
        when "10010" => return '1';
        when "10011" => return '1';
        when "10100" => return '1';
        when "10101" => return '1';
        when "10110" => return '1';
        when "10111" => return '0';
        when "11000" => return '1';
        when "11001" => return '1';
        when "11010" => return '1';
        when "11011" => return '0';
        when "11100" => return '1';
        when "11101" => return '0';
        when "11110" => return '0';
        when "11111" => return '0';
      end case;
    end function weak5;
    
    function invert12 (word12in: std_logic_vector(11 downto 0)) return std_logic_vector(11 downto 0) is
        variable word12out : std_logic_vector(11 downto 0);
    begin
        for bitnum in 11 downto 0 loop 
            word12out(bitnum) := word12in(11-bitnum);
        end loop;
        return word12out;
    end function invert12;
    
begin

isd : entity work.isampler
    PORT MAP (
        lvds_in_p => LVDS_in_p,
        lvds_in_n => LVDS_in_n,
        idelaycnt => idelay,
        iserdes_q => iserdes_q,
        clk_in => clk600,
        clk_div_in => clk120,
        io_reset => rst40
        );

process(clk120)
    variable isdq : std_logic_vector(16 downto 0);

    -- STEPS 1&2: Phase detector
    variable phase_detector         : unsigned(pd_bits-1 downto 0)            := to_unsigned(2**(pd_bits-1),pd_bits);
    variable edge                   : integer range 4 downto 0                    := 0; -- indica que las transiciones se dan entre el bit #edge-1 y el #edge
    variable offset, offset_old     : integer range 7 downto 0                    := 0; 
    variable countdown              : integer range 255 downto 0;
    -- STEP 2 TO 3
    variable newbits                : std_logic_vector(2 downto 0);
    variable newbitsweak            : std_logic_vector(2 downto 0);
    variable validbits		        : integer range 3 downto 0					:= 2;
    -- STEP 3: Circular buffer for word12 formation
    constant BBSIZE                 : integer := 16;
    variable bitbuf                 : std_logic_vector(BBSIZE-1 downto 0);
    variable bitbufweak             : std_logic_vector(BBSIZE-1 downto 0);
    variable bitbuf_num             : integer range BBSIZE-1 downto 0; -- also modified in step 4
    -- STEP 3 TO 4 
    variable word12                 : std_logic_vector(11 downto 0)			:= (others => '0');
    variable word12weak             : std_logic_vector(11 downto 0)			:= (others => '0');
    variable newword12              : boolean                                        := false;
    -- STEP 4: Locking to start-stop
    constant word_unlock_th 	: integer 								     := 16;
    constant word_unlock_inc    : integer                                    := 4; -- how much faster is unlocking than locking
    variable word_unlock_n      : integer range word_unlock_th-1 downto 0    := word_unlock_inc;
    variable lock12             : boolean := false;
    
    variable samples_v          : std_logic_vector(103 downto 0) := (others => '0');
    variable netedgeshift       : unsigned(7 downto 0) := (others => '0');
    variable edge_incdec        : std_logic_vector(1 downto 0);


begin
    if rising_edge(clk120) then
        -- Input to the process:
        isdq(16 downto 10) := isdq(6 downto 0);
        isdq(9 downto 0) := iserdes_q;
    
        -- STEP 4: Creation and alignment of the 12-bit word
        if newword12 then -- si entro aqu�, es que la vez anterior hubo palabra, y todavia esta vigente
            if (word12(0) = '0') or (word12(11) = '1') then
                if word_unlock_n >= word_unlock_th - word_unlock_inc then
                    word_unlock_n := word_unlock_inc;
                    bitbuf_num := bitbuf_num+1;-- Producir el bitslip:
                    lock12 := false;
                else 
                    word_unlock_n := word_unlock_n + word_unlock_inc;
                end if;
            elsif word_unlock_n > 0 then word_unlock_n := word_unlock_n - 1;
            else lock12 := true;
            end if;
            
            word12_toggle <= not word12_toggle;
            word12_data   <= word12;
            word12_weak   <= word12weak;
            word12_lock   <= bo2sl(lock12);
            samples       <= samples_v(103 downto 13);
        end if;
        
        -- STEP 3: Add new bits to the circular buffer and signal when 12 new bits are available
        if      validbits = 1 then  bitbuf := bitbuf(BBSIZE-2 downto 0) & newbits(0 downto 0);
                                    bitbufweak := bitbufweak(BBSIZE-2 downto 0) & newbitsweak(0 downto 0);
        elsif   validbits = 3 then  bitbuf := bitbuf(BBSIZE-4 downto 0) & newbits(2 downto 0);
                                    bitbufweak := bitbufweak(BBSIZE-4 downto 0) & newbitsweak(2 downto 0);
        else                        bitbuf := bitbuf(BBSIZE-3 downto 0) & newbits(1 downto 0);
                                    bitbufweak := bitbufweak(BBSIZE-3 downto 0) & newbitsweak(1 downto 0);
        end if;
    
        bitbuf_num := bitbuf_num + validbits;

        if bitbuf_num = 12 then
            word12 :=invert12(bitbuf(11 downto 0));
            word12weak :=invert12(bitbufweak(11 downto 0));
            bitbuf_num := 0;
            newword12 := true;
        elsif bitbuf_num = 13 then
            word12 :=invert12(bitbuf(12 downto 1));
            word12weak :=invert12(bitbufweak(12 downto 1));
            bitbuf_num := 1;
            newword12 := true;
        elsif bitbuf_num = 14 then
            word12 :=invert12(bitbuf(13 downto 2));
            word12weak :=invert12(bitbufweak(13 downto 2));
            bitbuf_num := 2;
            newword12 := true;
        else newword12 := false;
        end if;
        
        -- STEP 2: select the value for each bit opposed to the edge detected and add pass them to step 3
        offset_old := offset;
        offset := (edge + 2 - 2) mod 5 + 2;
--        newbits := majority(isdq(10+offset+1 downto 10+offset-1)) & majority(isdq(5+offset+1 downto 5+offset-1)) & majority(isdq(offset+1 downto offset-1));
        newbits := majority5(isdq(10+offset+2 downto 10+offset-2)) & majority5(isdq(5+offset+2 downto 5+offset-2)) & majority5(isdq(offset+2 downto offset-2));
        newbitsweak := weak5(isdq(10+offset+2 downto 10+offset-2)) & weak5(isdq(5+offset+2 downto 5+offset-2)) & weak5(isdq(offset+2 downto offset-2));
        
        if (offset_old = 6) and (offset = 2) then       validbits := 3;
        elsif (offset_old = 2) and (offset = 6) then    validbits := 1;
        else                                            validbits := 2;
        end if;

        -- STEP 1: Phase detector, checks if bit changes occur between bits edge-1 and edge
        edge_incdec := "00";
        if (phase_detector(pd_bits-1) = phase_detector(pd_inertia)) then -- STEP 1b
            if (phase_detector(pd_bits-1) = '0') then       edge := (edge - 1) mod 5;
                                                            edge_incdec(0) := '1';
            else                                            edge := (edge + 1) mod 5;
                                                            edge_incdec(1) := '1';
            end if;
            phase_detector := to_unsigned(2**(pd_bits-1),pd_bits);
        elsif countdown = 0 and pd_decay> 0 then
            countdown := pd_decay;
            phase_detector := phase_detector(pd_bits-1 downto pd_bits-2) & phase_detector(pd_bits-2 downto 1);
        else
            countdown := countdown -1;
            for j in 1 downto 0 loop
                if (isdq(5*j + edge + 2 downto 5*j + edge + 0) /= "111") and (isdq(5*j + edge + 2 downto 5*j + edge + 0) /= "000") then
                    phase_detector := phase_detector + 1;
                end if;
                if (isdq(5*j + edge + 4 downto 5*j + edge + 2) /= "111") and (isdq(5*j + edge + 4 downto 5*j + edge + 2) /= "000") then
                    phase_detector := phase_detector - 1;
                end if;
            end loop;
        end if;
        
        ----------------------------------------------
        -- DEBUG VARS
        samples_v := samples_v( samples_v'high-13 downto 0 ) & isdq(9 downto 0) & std_logic_vector(to_unsigned(offset,3));
        ctrinc_edgeshift <= bo2sl(edge_incdec /= "00");
        netedgeshift := netedgeshift + bo2int(edge_incdec = "10") - bo2int(edge_incdec = "01");
        stats.netedgeshift <= netedgeshift;
        -- DEBUG VARS
        ------------------------------------------------- 
    end if;
end process;

stats.w12_data    <= word12_data   ;
stats.w12_weak    <= word12_weak   ;
stats.w12_samples <= samples       ;
stats.w12_toggle  <= word12_toggle ;
stats.w12_lock    <= word12_lock   ;

process(clk40)
    
    variable word12_ack             : std_logic;
    variable word12_lock_old        : std_logic;
    variable word12_data_v          : std_logic_vector(11 downto 0);
    variable word12_weak_v          : std_logic_vector(11 downto 0);
    variable samples_v              : std_logic_vector(90 downto 0);
    variable word8_data             : std_logic_vector(7 downto 0);
    variable word9_weak, word9_data : std_logic_vector(8 downto 0);
    variable word9_fixable          : boolean;
    variable word8_valid            : boolean;
    variable word9_parity_error     : boolean;
    variable word12_startstop_error : boolean;
    variable dostep1,dostep2,dostep3,dostep4 : boolean := false;

    variable bytepos        : unsigned(1 downto 0) := "11";
    variable word32_lock    : std_logic := '0';
    variable word32_data    : std_logic_vector(31 downto 0) := (others => '0');
    variable word32_valid   : boolean := false;
    variable sofarsogood    : boolean := false;
    variable do_wren        : boolean := false;
    variable ff_overflow_ew : boolean := false;
    variable wren_hold      : boolean := true;
    variable in_event       : boolean := false;
    variable word_ctr       : unsigned(11 downto 0);
    variable current_evn    : unsigned(11 downto 0);
    variable inevent_idles  : unsigned(5 downto 0);
    
    variable robff_di_v     : std_logic_vector(31 downto 0) := (others => '0');
    
begin
    if rising_edge(clk40) then
        -- DEBUG vars
        ctrinc_unlock12        <= '0' ;
        ctrinc_startstoperr    <= '0' ;
        ctrinc_parityerr_total <= '0' ;
        ctrinc_parityerr_nofix <= '0' ;
        ctrinc_valid           <= '0' ;
        ctrinc_w32interrupt    <= '0' ;
        ctrinc_w32valid        <= '0' ;
        ctrinc_inevent_idles   <= '0' ;
        ctrinc_header          <= '0' ;
        ctrinc_trailer         <= '0' ;
        ctrinc_hits            <= '0' ;
        ctrinc_errs            <= '0' ;
        ctrinc_unknown_robword <= '0' ;
        ctrinc_not_in_event    <= '0' ;
        ctrinc_nonconsecutive  <= '0' ;
        ctrinc_notrailer       <= '0' ;
        ctrinc_badtrailer_evn  <= '0' ;
        ctrinc_badtrailer_wrd  <= '0' ;
        ctrinc_fifo_ofw_losses <= '0' ;
        ctrinc_wren            <= '0' ;
        -------------
        robff_wren <= '0';
        word32_lock := word32_lock and word12_lock; -- if not locked, unlock the 32 word

        --- STEP2 -- lock to, create and validate word32 from 4 consecutive word8's
--        dostep3 := dostep2;
        if dostep2 then
            word32_valid := false;
            word32_data(8*to_integer(bytepos) + 7 downto 8*to_integer(bytepos)) := word8_data;
            inevent_idles := inevent_idles + bo2int( not word8_valid and inevent_idles /= (inevent_idles'range => '1') );
            if word9_parity_error and not word9_fixable then 
                if word8_valid then
                    word32_lock := '0';
                    ctrinc_parityerr_nofix    <= '1';
                end if;
            elsif word32_lock = '0' then -- parity is ok, but we are lost count of where the word32 starts
                bytepos := "11";
                if not word8_valid then word32_lock := '1';
                end if;
            elsif bytepos = 3 and word8_valid then -- parity is ok, and locked to word32, and then a new word 32 starts
                bytepos := bytepos - 1 ; 
            elsif bytepos /= 3 then-- parity is ok, locked to word32, a new word32 has already started ==> it must continue immediately
                if word8_valid then 
                    if bytepos = 0 then word32_valid := true;
                    end if;
                    bytepos := bytepos - 1;
                else
                    word32_lock := '0';
                    ctrinc_w32interrupt <= '1';
                end if;
            --else null; this case would correspond to no parity error, locked to 32-bit word, waiting for the start of the new word32 but receiving idles
            end if;
            
            ctrinc_valid             <= bo2sl( word8_valid );
            ctrinc_startstoperr      <= bo2sl( word12_startstop_error );
            ctrinc_parityerr_total   <= bo2sl( word9_parity_error and word8_valid );
            
--        end if;
        

        --- STEP 3 -- Analyze the ROB data stream and prepare the write to fifo 
--        dostep4 := dostep3;
--        if dostep3 then
            do_wren := false;
            if word32_lock = '0' then
                if sofarsogood then
                    robff_di_v := ERROR_WORD_WORD32_UNLOCK;
                    do_wren := true;
                end if;
                sofarsogood := false;
            elsif word32_valid and word32_data(31 downto 28) = x"0" then -- w32_lock & w32_valid & header
                ctrinc_notrailer <= bo2sl( sofarsogood and in_event ); -- F word can't be inserted because new header has to
                ctrinc_nonconsecutive <= bo2sl( (unsigned(word32_data(23 downto 12)) /= x"000") and -- ROB events start in 0
                                               (unsigned(word32_data(23 downto 12)) /= current_evn + 1));
                ctrinc_header         <= '1';
                sofarsogood := true;
                in_event := true; 
                current_evn := unsigned(word32_data(23 downto 12));
                word_ctr := (0=> '1',others => '0');
                inevent_idles := (others => '0');
                robff_di_v :=  word32_data;
                do_wren := true;
            elsif sofarsogood and in_event and inevent_idles > max_inevent_idles then -- w32_lock & not header & sfsg & in_event & too much idle
                robff_di_v := ERROR_INEVENT_IDLES;
                sofarsogood := false;
                ctrinc_inevent_idles     <= '1';
                do_wren := true;
            elsif word32_valid and sofarsogood then -- w32_lock & w32_valid & not header & sfsg 
                word_ctr := word_ctr + 1;
                robff_di_v :=  word32_data;
                if not in_event then
                    robff_di_v := ERROR_WORD_OUTSIDE_EVENT;
                    sofarsogood := false;
                    ctrinc_not_in_event     <= '1';
                elsif word32_data(31 downto 28) = x"1" then --trailer
                    if word32_data(23 downto 12) /= std_logic_vector(current_evn) then
                        robff_di_v := ERROR_WORD_TRAILER_EVN_BAD;
                        sofarsogood := false;
                        ctrinc_badtrailer_evn   <= '1';
                    elsif word32_data(11 downto 0) /= std_logic_vector(word_ctr) then
                        robff_di_v := ERROR_WORD_TRAILER_CNT_BAD;
                        sofarsogood := false;
                        ctrinc_badtrailer_wrd   <= '1';
                    else ctrinc_trailer <= '1';
                    end if;
                    in_event := false;
                elsif word32_data(31 downto 28) = x"4" then ctrinc_hits <= '1';
                elsif word32_data(31 downto 28) = x"6" then ctrinc_errs <= '1';
                else
                    robff_di_v := ERROR_WORD_UNKOWN_ROBWORD;
                    sofarsogood := false;
                    ctrinc_unknown_robword  <= '1';
                end if;
                do_wren := true;
            end if;

            ctrinc_w32valid <= bo2sl( word32_valid ) ;

--        end if;
        
                        
        --- STEP 4 -- do the actual write to fifo
--        if dostep4 then
            if robff_full = '0' and ff_overflow_ew then -- it's possible to write and there's a ff_overflow_ew report pending
                robff_di <= ERROR_WORD_FIFO_OVERFLOW;
                robff_wren <= '1';
                ff_overflow_ew := false;
            elsif robff_full = '0' and do_wren and (wren_hold = false or robff_di_v(31 downto 28) = x"0") then -- it's possible to write and there's something to write --> OK
                robff_di <= robff_di_v;
                robff_wren <= '1';
                wren_hold := false;
                ctrinc_wren <= '1';
            elsif robff_full = '1' and do_wren then -- it's not possible to write so somethings going to be lost
                ff_overflow_ew := true;
                wren_hold := true; -- wren will be started again when new header comes
            end if;
            
            ctrinc_fifo_ofw_losses   <= bo2sl( do_wren and wren_hold );
            
        end if;

        --- STEP 0 decission
        dostep1 := ((word12_toggle xor word12_ack) and word12_lock) = '1';
        word12_ack     := word12_toggle;
        
        word12_data_v  := word12_data;
        word12_weak_v  := word12_weak;
        samples_v      := samples;

        --- STEP 1 data manipulation
        dostep2 := dostep1;
--        if dostep1 then
            word12_startstop_error  := (word12_data_v(0) = '0') or (word12_data_v(11) = '1');
            word8_valid             := word12_data_v(10) = '1';

            word9_data              := word12_data_v(9 downto 1) ;
            word9_weak              := word12_weak_v(9 downto 1) ;
            word9_parity_error      := ( word12_data_v(1) xor word12_data_v(2) xor word12_data_v(3) xor 
                                         word12_data_v(4) xor word12_data_v(5) xor word12_data_v(6) xor 
                                         word12_data_v(7) xor word12_data_v(8) xor word12_data_v(9) ) = '1';
            word9_fixable           :=  (word9_weak = "100000000") or (word9_weak = "010000000") or (word9_weak = "001000000") or
                                        (word9_weak = "000100000") or (word9_weak = "000010000") or (word9_weak = "000001000") or
                                        (word9_weak = "000000100") or (word9_weak = "000000010") or (word9_weak = "000000001");
            if (word9_parity_error and word9_fixable) then  word8_data := word9_data(7 downto 0) xor word9_weak(7 downto 0);
            else                                            word8_data := word9_data(7 downto 0);
            end if;

            if word9_parity_error or word12_startstop_error then
                stats.w12_samples_snapshot <= samples_v;
                stats.w12_data_snapshot <= word12_data_v;
                stats.w12_weak_snapshot <= word12_weak_v;
            end if;

--        end if;

        --------------------------------------
        --- DEBUG related tasks
        ctrinc_unlock12 <= word12_lock_old and not word12_lock ;
        word12_lock_old := word12_lock;
        -- DEBUG VARS
        --------------------------------------

    end if;
end process;



stats.counters_inc( 0) <= ctrinc_edgeshift       ; 
stats.counters_inc( 1) <= ctrinc_unlock12        ; 
stats.counters_inc( 2) <= ctrinc_startstoperr    ; 
stats.counters_inc( 3) <= ctrinc_parityerr_total ; 
stats.counters_inc( 4) <= ctrinc_parityerr_nofix ; 
stats.counters_inc( 5) <= ctrinc_valid           ; 
stats.counters_inc( 6) <= ctrinc_w32interrupt    ; 
stats.counters_inc( 7) <= ctrinc_w32valid        ; 
stats.counters_inc( 8) <= ctrinc_inevent_idles   ; 
stats.counters_inc( 9) <= ctrinc_header          ; 
stats.counters_inc(10) <= ctrinc_trailer         ; 
stats.counters_inc(11) <= ctrinc_hits            ; 
stats.counters_inc(12) <= ctrinc_errs            ; 
stats.counters_inc(13) <= ctrinc_unknown_robword ; 
stats.counters_inc(14) <= ctrinc_not_in_event    ; 
stats.counters_inc(15) <= ctrinc_nonconsecutive  ; 
stats.counters_inc(16) <= ctrinc_notrailer       ; 
stats.counters_inc(17) <= ctrinc_badtrailer_evn  ; 
stats.counters_inc(18) <= ctrinc_badtrailer_wrd  ; 
stats.counters_inc(19) <= ctrinc_fifo_ofw_losses ; 
stats.counters_inc(20) <= ctrinc_wren            ; 
stats.counters_inc(21) <= ctrinc_fifo_af         ; 
    

robff : entity work.qwfifo
generic map (
    DATA_WIDTH => 32,
    DEPTH_LOG2 => ROBFIFO_DEPTH)
port map (
    ALMOSTEMPTY => open,
    ALMOSTFULL => open,
    RDCOUNT => rdctr,
    WRCOUNT => wrctr,
    
    RST => rst40,                   -- 1-bit input reset
    
    WRCLK => clk40,               -- 1-bit input write clock
    FULL => robff_full,                 -- 1-bit output full
    WREN => robff_wren,                  -- 1-bit input write enable
    DI => robff_di, -- Input data, width defined by DATA_WIDTH parameter
    
    RDCLK => robff_rdclk,               -- 1-bit input read clock
    EMPTY => robff_empty,       -- 1-bit output empty
    RDEN => robff_rden,         -- 1-bit input read enable
    DO => robff_do              -- Output data, width defined by DATA_WIDTH parameter
);

process (clk40)
    variable occupancy, max_occupancy : unsigned(ROBFIFO_DEPTH-1 downto 0);
begin
    if rising_edge(clk40) then
        occupancy := unsigned(wrctr) - unsigned(rdctr);
        ctrinc_fifo_af <= bo2sl( occupancy > conf_rob_af_thres );
        if conf_ctr_rst = '1' then  max_occupancy := (others => '0');
        else                        max_occupancy := maximum(max_occupancy, occupancy);
        end if;
        stats.ff_max_occupancy <= max_occupancy;
    end if;
end process;

    
end Behavioral;
