#!/bin/env python
address_table_file = 'uros_readout.xml'

import os, sys

# this can be suppressed but you'll have to export LD_LIBRARY_PATH before calling the script
if 'LD_LIBRARY_PATH' not in os.environ:
    os.environ['LD_LIBRARY_PATH'] = '/opt/cactus/lib'
    try:
        os.execv(sys.argv[0], sys.argv)
    except Exception, exc:
        print 'Failed re-exec:', exc
        sys.exit(1)


import uhal
import subprocess

os.environ['LD_LIBRARY_PATH'] = "/opt/cactus/lib/"

# this is to capture the warnings printed and read the filename from there
p = subprocess.Popen('''export LD_LIBRARY_PATH=/opt/cactus/lib; python -c "import uhal; uhal.setLogLevelTo(uhal.LogLevel.WARNING); dev = uhal.getDevice('test','ipbusudp-2.0://localhost:50001', 'file://%s')"'''%address_table_file, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
warns = p.stdout.read()

uhal.setLogLevelTo(uhal.LogLevel.WARNING)
dev = uhal.getDevice('test','ipbusudp-2.0://localhost:50001', 'file://%s'%address_table_file)

if 'WARNING - Address overlaps observed' in warns:
    overlapreportfilename = warns.split('report file written at "')[1].split('"')[0]
    with open(overlapreportfilename) as f:
        print ' ' + '_'*10 + ' ' +overlapreportfilename + ' ' +'_'*50 +'\n/'
        for l in f.readlines(): print '| '+ l,
        print '\\' + '_'*(62 + len(overlapreportfilename)) + '\n\n'

print 'Top-level nodes address-space report:'
for toplevel in sorted([n for n in dev.getNodes() if '.' not in n]):
    print '    "%s" extends from 0x%X to 0x%X'%( toplevel,
                                           min([dev.getNode(toplevel+'.'+n).getAddress() for n in dev.getNode(toplevel).getNodes()]),
                                           max([dev.getNode(toplevel+'.'+n).getAddress() for n in dev.getNode(toplevel).getNodes()]),
                                          )

def parseMask(mask, name):
    lsb = 0
    if mask == 0:
        raise Exception('Node "%s" has invalid mask (0x%08X)'%(name,mask))
    while not (mask & (1<<lsb)):
        lsb += 1
    msb = lsb
    while (mask & (2<<msb)):
        msb += 1
    if mask - ( (2<<msb) - (1<<lsb) ) :
        raise Exception('Node "%s" has invalid mask (0x%08X)'%(name,mask))
    return msb, lsb
    

def add_signals_from_branch(dev, branchname, signals):
    node = dev.getNode(branchname)
    children = [(branchname+'.'+child).strip('.') for child in node.getNodes() if '.' not in child]
    for child in children:
        childNode = dev.getNode(child)
        if 'nodivide' in childNode.getTags().split(';') or not childNode.getNodes():
            signalName = child.replace('.','_')
            msb, lsb = parseMask(childNode.getMask(), child)
            taglist = childNode.getTags().split(';') 
            signalType = 'sl' if msb == lsb else 'std_logic_vector' if 'slv' in taglist else 'signed' if 'sgn' in taglist else 'unsigned'
            fragment = 'fragment' in childNode.getTags().split(';')

            if signalName in signals:
                raise Exception("Signal name after dot to underscore substitution duplication found: %s"%signalName)
            signals[signalName] = {'type':signalType, 'regs':[{'num':childNode.getAddress(),'msb':msb, 'lsb':lsb}] }
            for tag in childNode.getTags().split(';'):
                if tag in ['nodeclare','noassign']:
                    signals[signalName][tag] = tag
                if tag[:9]=='fragment=':
                    try:
                        signals[signalName]['fragment'] = int(tag[9:])
                    except ValueError:
                        raise Exception('Invalid fragment tag: "%s"'%tag)
        else:
            add_signals_from_branch(dev, child, signals)

signals = {}
add_signals_from_branch(dev,'', signals)

while any(['fragment' in signals[signalName] for signalName in signals]):
    lowest_fragment = min([ signals[signalName]['fragment'] for signalName in signals if 'fragment' in signals[signalName] ])
    for signalName in signals:
        if 'fragment' in signals[signalName] and signals[signalName]['fragment'] == lowest_fragment:
            if lowest_fragment < 0 :
                raise Exception('Invalid fragment index %i in register "%s"'%(signals[signalName]['fragment'], signalName) )
            baseName = '_'.join(signalName.split('_')[:-1])
            if signals[signalName]['fragment'] == 0:
                if baseName in signals:
                    raise Exception('Error removing latest _token from regname "%s": signal name "%s" already exists'%(signalName, baseName))
                signals[baseName] = signals.pop(signalName)
                signals[baseName].pop('fragment')
                break
            else:
                if baseName not in signals:
                    raise Exception('Non-primary fragment "%s" does not correspond to an existing primary fragment with base "%s"'%(signalName, baseName) )
                elif signals[baseName]['type']!=signals[signalName]['type']:
                    raise Exception('Non-primary fragment "%s" does not match to previously added fragment(s)'%signalName)
                elif len(signals[baseName]['regs']) != signals[signalName]['fragment']:
                    raise Exception('Missing or repeated fragment for register "%s"'%signalName)
                signals[baseName]['regs'] += signals[signalName]['regs']
                signals.pop(signalName)
                break

# Code for signal declarations
decltxt = ''
if any([signals[s]['type']=='sl' for s in signals]):
    decltxt += '    signal ' + ', '.join(sorted([s for s in signals if (signals[s]['type']=='sl' and not 'nodeclare' in signals[s])])) +' : std_logic := \'0\';\n'

vectors = {'std_logic_vector':{}, 'unsigned':{}, 'signed':{}}
for s in signals:
    if signals[s]['type'] in ['std_logic_vector','unsigned','signed'] and not 'nodeclare' in signals[s]:
        wid = sum([ reg['msb']+1-reg['lsb'] for reg in signals[s]['regs'] ])
        if not wid in vectors[signals[s]['type']]:
            vectors[signals[s]['type']][wid] = [s]
        else:
            vectors[signals[s]['type']][wid] += [s]

for sigtype in sorted(vectors):
    decltxt+='\n'
    for wid in sorted(vectors[sigtype]):
        decltxt += '    signal ' + ', '.join(sorted( vectors[sigtype][wid] )) +' : %s(%i downto 0) := (others => \'0\');\n'%(sigtype,wid-1)

        
        
# Code for signal assignment        
assigntxt = ''
for s in sorted(signals, key=lambda s: "%02X%02i"%(signals[s]['regs'][0]['num'],  signals[s]['regs'][0]['lsb']) ):
    if 'nodeclare' in signals[s] or 'noassign' in signals[s]: continue
    passedbits = 0
    for r in range(len(signals[s]['regs'])):
        wid = signals[s]['regs'][r]['msb'] + 1 - signals[s]['regs'][r]['lsb']
        
        signalrange = '(%i downto %i)'%(passedbits + wid -1, passedbits) if len(signals[s]['regs'])>1 else ''
        converteropen = '' if 'signed' not in signals[s]['type'] else signals[s]['type']+'( ' if s[:4] == 'conf' else 'std_logic_vector( '
        converterclose = '' if 'signed' not in signals[s]['type'] else ' )'
        reginhex = '16#%02X#'%(signals[s]['regs'][r]['num'] - (0 if s[:4] == 'conf' else 0x40) )
        regrange = '(%i%s)'%(signals[s]['regs'][r]['msb'], '' if wid == 1 else ' downto %i'%signals[s]['regs'][r]['lsb'])
        
        if s[:4] == 'conf':
            assigntxt += '    %s%s <= %sconf(%s)%s%s;\n'%\
              ( s, signalrange, converteropen, reginhex, regrange, converterclose )
        elif s[:4] == 'stat':
            assigntxt += '    stat(%s)%s <= %s%s%s%s;\n'%\
              (reginhex, regrange, converteropen, s, signalrange, converterclose, )
              
        passedbits += wid
    
align = max([ l.find('<=') for l in assigntxt.split('\n') ]) 
assigntxt = '\n'.join([ l.replace('<=',' '*(align-l.find('<='))+'<=') for l in assigntxt.split('\n') ])

# Fix long lines
def fixlonglines(txt, threshold = 120):
    lines = txt.split('\n')
    lines2 = []
    while lines:
        if len(lines[0]) > threshold and ', ' in lines[0]:
            if lines[0].rfind(', ',0,threshold) != -1:
                splitat = lines[0].rfind(', ',0,threshold)
            else:
                splitat = lines[0].find(', ')
            lines2 += [lines[0][:splitat]+',']
            lines = ['        '+lines[0][splitat+2:]] + lines[1:]
        else:
            lines2 += [lines[0]]
            lines = lines[1:]
    return '\n'.join(lines2)


print  '\n'*4 + '    ' +'-'*80 + '\n    -- Start of automatically-generated VHDL code for register "breakout" signals declaration'
print(fixlonglines(decltxt))
print '    -- End of automatically-generated VHDL code for register "breakout" signals declaration\n    ' + '-'*80 + '\n'*4

print 'begin'

print  '\n'*4 + '    ' +'-'*80 + '\n    -- Start of automatically-generated VHDL code for register "breakout" signals assignment'
print(fixlonglines(assigntxt))
print '    -- End of automatically-generated VHDL code for register "breakout" signals assignment\n    ' + '-'*80 + '\n'*4



