pattern = '''
    <node address="0x%s" mask="0x" id="rob%s"  module="file://uros_readout_inputConf.xml"/>'''


base_addr = 0x0
base_mask = 0
regs = [{'name':'pd_inertia_%02i', 'width':3},
        {'name':'pd_decay_%02i'  , 'width':8},
        {'name':'idelay_%02i'    , 'width':5},
        ]

for rob in range(72):
    for reg in regs:
        if base_mask + reg['width']>33:
            base_addr += 1
            base_mask = 0
        addr = '0x%02X'%base_addr
        mask = '0x%08X'%((2**reg['width']-1)<<base_mask)
        name = reg['name']%rob
        print '  '*3 + '<node address="%s" mask="%s" tags="slv;fragment=%02i" id="%s"/>'%(addr, mask, rob, name)

        base_mask += reg['width']
