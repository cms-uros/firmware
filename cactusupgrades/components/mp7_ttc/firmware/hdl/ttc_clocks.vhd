-- ttc_clocks
--
-- Clock generation for LHC clocks
--
-- Dave Newbold, June 2013

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library unisim;
use unisim.VComponents.all;

entity ttc_clocks is
	port(
		clk40_in_p: in std_logic;		
		clk40_in_n: in std_logic;
		clk_p40: in std_logic;
		clko_20: out std_logic;
		clko_40: out std_logic;
		clko_80: out std_logic;
		clko_120: out std_logic;
		clko_200: out std_logic;
--		clko_240: out std_logic;
		clko_600: out std_logic;
		rsto_20: out std_logic;
		rsto_40: out std_logic;
		rsto_80: out std_logic;
		rsto_120: out std_logic;
		rsto_200: out std_logic;
--		rsto_240: out std_logic;
		clko_40s: out std_logic;
		stopped: out std_logic;
		locked: out std_logic;
		rst_mmcm: in std_logic;
		rsti: in std_logic;
		clksel: in std_logic;
		psen: in std_logic;
		psval: in std_logic_vector(11 downto 0);
		psok: out std_logic
	);

end ttc_clocks;

architecture rtl of ttc_clocks is

	signal clk40_bp, clk40_bp_u, clk_fb, clk_fb_fr, clk_p40_b: std_logic;
	signal clk20_u, clk40_u, clk80_u, clk120_u, clk200_u, clk40s_u, clk600_u, clk20_i, clk40_i, clk80_i, clk120_i, clk200_i, clk600_i: std_logic;
	signal locked_i, rsto_80_r, rsto_120_r, rsto_200_r: std_logic;
	signal pscur_i: std_logic_vector(11 downto 0);
	signal psrst, psgo, psincdec, psdone, psbusy, psdiff: std_logic;
	
begin

-- Input buffers

	ibuf_clk40: IBUFGDS
		port map(
			i => clk40_in_p,
			ib => clk40_in_n,
			o => clk40_bp_u
		);
		
	bufr_clk40: BUFG
		port map(
			i => clk40_bp_u,
			o => clk40_bp
		);
		
-- MMCM

	mmcm: MMCME2_ADV
	    generic map(
            --BANDWIDTH            => "OPTIMIZED",
            --CLKOUT4_CASCADE      => FALSE,
            --COMPENSATION         => "ZHOLD",
            --STARTUP_WAIT         => FALSE,
            DIVCLK_DIVIDE        => 1,
            CLKFBOUT_MULT_F      => 30.000,
            CLKFBOUT_PHASE       => 0.000,
            --CLKFBOUT_USE_FINE_PS => FALSE,
                    
            CLKIN1_PERIOD        => 25.000,
            CLKIN2_PERIOD        => 25.000,
            REF_JITTER1          => 0.010,
            
            CLKOUT1_DIVIDE       => 30,
            CLKOUT1_PHASE        => 0.000,
            CLKOUT1_DUTY_CYCLE   => 0.500,
            --CLKOUT1_USE_FINE_PS  => FALSE,
            
            CLKOUT2_DIVIDE       => 30,
            CLKOUT2_PHASE        => 45.00,
            CLKOUT2_DUTY_CYCLE   => 0.500,
            CLKOUT2_USE_FINE_PS  => TRUE,
            
            CLKOUT0_DIVIDE_F     => 2.000,
            CLKOUT0_PHASE        => 0.000,
            CLKOUT0_DUTY_CYCLE   => 0.500,
            
            CLKOUT3_DIVIDE       => 15,
            CLKOUT3_PHASE        => 0.000,
            CLKOUT3_DUTY_CYCLE   => 0.500,    
            
            CLKOUT4_DIVIDE       => 6,
            CLKOUT4_PHASE        => 0.000,
            CLKOUT4_DUTY_CYCLE   => 0.500,
            
            CLKOUT5_DIVIDE       => 60,
            CLKOUT5_PHASE        => 0.000,
            CLKOUT5_DUTY_CYCLE   => 0.500,
            
            CLKOUT6_DIVIDE       => 10,
            CLKOUT6_PHASE        => 0.000,
            CLKOUT6_DUTY_CYCLE   => 0.500)
		port map(
			clkin1 => clk40_bp,
			clkin2 => clk_p40,
			clkinsel => clksel,
			clkfbin => clk_fb,
			clkfbout => clk_fb,
			clkout0 => clk600_u,
			clkout1 => clk40_u,
			clkout2 => clk40s_u,
			clkout3 => clk80_u,
			clkout4 => clk200_u,
			clkout5 => clk20_u,
			clkout6 => clk120_u,
			rst => rst_mmcm,
			pwrdwn => '0',
			clkinstopped => stopped,
			locked => locked_i,
			daddr => "0000000",
			di => X"0000",
			dwe => '0',
			den => '0',
			dclk => '0',
			psclk => clk40_i,
			psen => psgo,
			psincdec => psincdec,
			psdone => psdone
		);
		
	locked <= locked_i;

-- Phase shift state machine
	
	psrst <= rst_mmcm or not locked_i or not psen;

	process(clk40_i)
	begin
		if rising_edge(clk40_i) then

			if psrst = '1' then
				pscur_i <= X"000";
			elsif psdone = '1' then
				if psincdec = '1' then
					pscur_i <= std_logic_vector(unsigned(pscur_i) + 1);
				else
					pscur_i <= std_logic_vector(unsigned(pscur_i) - 1);
				end if;
			end if;

			psgo <= psdiff and not (psbusy or psgo or psrst);
			psbusy <= ((psbusy and not psdone) or psgo) and not psrst;

		end if;
	end process;
	
	psincdec <= '1' when psval > pscur_i else '0';
	psdiff <= '1' when psval /= pscur_i else '0';
	psok <= not psdiff;
	
-- Buffers
	
	bufg_20: BUFG
		port map(
			i => clk20_u,
			o => clk20_i
		);
		
	clko_20 <= clk20_i;

	process(clk20_i)
	begin
		if rising_edge(clk20_i) then
			rsto_20 <= rsti or not locked_i;
		end if;
	end process;
	
	bufg_40: BUFG
		port map(
			i => clk40_u,
			o => clk40_i
		);
		
	clko_40 <= clk40_i;

	process(clk40_i)
	begin
		if rising_edge(clk40_i) then
			rsto_40 <= rsti or not locked_i;
		end if;
	end process;

	bufg_80: BUFG
		port map(
			i => clk80_u,
			o => clk80_i
		);
		
	clko_80 <= clk80_i;
		
	process(clk80_i)
	begin
		if rising_edge(clk80_i) then
			rsto_80_r <= rsti or not locked_i; -- Disaster looms if tools duplicate this signal
			rsto_80 <= rsto_80_r; -- Pipelining for high-fanout signal
		end if;
	end process;

	bufg_120: BUFG
		port map(
			i => clk120_u,
			o => clk120_i
		);
		
	clko_120 <= clk120_i;
		
	process(clk120_i)
	begin
		if rising_edge(clk120_i) then
			rsto_120_r <= rsti or not locked_i; -- Disaster looms if tools duplicate this signal
			rsto_120 <= rsto_120_r; -- Pipelining for high-fanout signal
		end if;
	end process;
		
	bufg_200: BUFG
		port map(
			i => clk200_u,
			o => clk200_i
		);
		
	clko_200 <= clk200_i;
		
	process(clk200_i)
	begin
		if rising_edge(clk200_i) then
			rsto_200_r <= rsti or not locked_i; -- Disaster looms if tools duplicate this signal
			rsto_200 <= rsto_200_r; -- Pipelining for high-fanout signal
		end if;
	end process;
		
	bufg_600: BUFG
          port map(
              i => clk600_u,
              o => clk600_i
           );
                
    clko_600 <= clk600_i;
	
--	bufr_40s: BUFH
	bufr_40s: BUFG
		port map(
			i => clk40s_u,
			o => clko_40s
		);

end rtl;
