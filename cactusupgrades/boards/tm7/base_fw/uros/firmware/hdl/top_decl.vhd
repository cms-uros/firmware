-- Modified for TM7; original in projects/examples/mp7_690es/firmware/hdl

-- top_decl
--
-- Defines constants for the whole device
--
-- Dave Newbold, June 2014

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

use work.mp7_top_decl.all;
use work.uROS_version.all;

package top_decl is
    
    -- uROS role identifier: 
	constant ALGO_REV: std_logic_vector(31 downto 0) := X"02" & std_logic_vector(to_unsigned(SVN_REVISION,24));
--	constant BUILDSYS_BUILD_TIME: std_logic_vector(31 downto 0) := X"00000000"; -- To be overwritten at build time
--	constant BUILDSYS_BLAME_HASH: std_logic_vector(31 downto 0) := X"00000000"; -- To be overwritten at build time
	
	constant LHC_BUNCH_COUNT: integer := 3564;
	constant LB_ADDR_WIDTH: integer := 10;
--	constant DR_ADDR_WIDTH: integer := 9;
	constant RO_CHUNKS: integer := 32;
	constant CLOCK_RATIO: integer := 6;
	constant CLOCK_RATIO_PAYLOAD: integer := 6;
--	constant PAYLOAD_LATENCY: integer := 2;
--	constant DAQ_N_BANKS: integer := 4; -- Number of readout banks
--	constant DAQ_TRIGGER_MODES: integer := 2; -- Number of trigger modes for readout
--	constant DAQ_N_CAP_CTRLS: integer := 4; -- Number of capture controls per trigger mode

--  -- Alternative refclk (4th entry in table) is 
--  -- not used in R1 card, but is used in XE.
--  -- 3rd column is therefore just duplicated.
  
--	constant REGION_CONF: region_conf_array_t := (
--		(gth_10g_std_lat, no_chk, no_buf, demux, buf, u_crc32, gth_10g_std_lat, 0, 0), -- 0 / 118
--		(gth_10g_std_lat, no_chk, no_buf, demux, buf, u_crc32, gth_10g_std_lat, 0, 0), -- 1 / 117*
--		(gth_10g_std_lat, no_chk, no_buf, demux, buf, u_crc32, gth_10g_std_lat, 0, 0)  -- 2 / 116
--	);

end top_decl;
