-- Top-level design for TM7 base firmware
--
-- Dave Newbold, July 2012
-- Nikitas Loukas, March 2016
-- Javier Sastre, May 2016

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.ipbus.all;
use work.ipbus_trans_decl.all;
use work.mp7_ttc_decl.all;
use work.mp7_brd_decl.all;

use work.uros_readout_decl.all;

entity top is
    port(
        eth_clkp, eth_clkn: in std_logic;
        eth_txp, eth_txn: out std_logic;
        eth_rxp, eth_rxn: in std_logic;
        led1_yellow,led1_green,led1_red,led1_orange: out std_logic; -- on mp7 leds 1,0,10,9 {led_q(4:0)}
        led2_yellow,led2_green,led2_red,led2_orange: out std_logic; -- on mp7 leds 1,0,n,n  {not(locked and onehz),locked)
        clk40_in_p: in std_logic;
        clk40_in_n: in std_logic;
        ttc_in_p: in std_logic;
        ttc_in_n: in std_logic;
        reset_mp_rx_hr,reset_mp_tx_hr: out std_logic;
        refclkp: in std_logic_vector(N_REFCLK - 1 downto 0);
        refclkn: in std_logic_vector(N_REFCLK - 1 downto 0);
        
        LVDS_in_p : in  STD_LOGIC_VECTOR(71 downto 0);  --DT    
        LVDS_in_n : in  STD_LOGIC_VECTOR(71 downto 0);  --DT
        clk_100 : in  STD_LOGIC;
        SN_RXEN_HR : out  STD_LOGIC_VECTOR(5 downto 0);
        SN_SD_HR : in  STD_LOGIC_VECTOR(5 downto 0);
        SN_SQEN_HR : out  STD_LOGIC_VECTOR(5 downto 0);
        SN_ENSD_HR : out  STD_LOGIC_VECTOR(5 downto 0);
        SEL0_MGT_CLK_18 : out  std_logic;
        GA : in  STD_LOGIC_VECTOR(3 downto 0);
        dipswitch : in  STD_LOGIC_VECTOR(7 downto 0);
        AMC_P1_RX_P, AMC_P1_RX_N : in std_logic;  --JSA--
        AMC_P1_TX_N, AMC_P1_TX_P : out std_logic; --JSA--        
        SEL1_MGT_CLK_18 : out  std_logic;
        --MGT_CLK_115_1_P,MGT_CLK_115_1_N : in STD_LOGIC;
        ------------------------------------------------------------------------
        -- SPI flash memory ports
        -- outSpiClk is output through STARTUPE2.USRCCLKO
        outSpiCsB           : out std_logic; -- Spi Chip Select. Connect to SPI Flash chip-select via FPGA FCS_B pin
        outSpiMosi          : out std_logic; -- Spi MOSI. Connect to SPI DQ0 pin via the FPGA D00_MOSI pin
        inSpiMiso           : in  std_logic; -- Spi MISO. Connect to SPI DQ1 pin via the FPGA D01_DIN pin
        outSpiWpB           : out std_logic; -- SPI flash write protect. Connect to SPI 'W_B/Vpp/DQ2'
        outSpiHoldB         : out std_logic  -- Connect to SPI 'HOLD_B/DQ3'. Fix to '1'.
    );

end top;

architecture rtl of top is
    
    signal clk_ipb, rst_ipb, clk40ish, clk40, rst40, clk600, clk120, clk200, clk80, clk20, eth_refclk: std_logic;
    signal clk40_rst, clk40_sel, clk40_lock, clk40_stop, nuke, soft_rst: std_logic;
    
    signal ipb_in_ctrl, ipb_in_ttc, ipb_in_spiprogrammer, ipb_in_readout : ipb_wbus;
    signal ipb_out_ctrl, ipb_out_ttc, ipb_out_spiprogrammer, ipb_out_readout : ipb_rbus;
    
    signal board_id: std_logic_vector(31 downto 0);
    signal ttc_l1a, dist_lock, oc_flag, ec_flag, ttc_l1a_throttle, ttc_l1a_flag, ttc_l1a_dist: std_logic;
    signal ttc_cmd, ttc_cmd_dist: ttc_cmd_t;
    signal bunch_ctr_mp7: bctr_t;
    --signal bunch_ctr: bctr_t;
    signal evt_ctr, orb_ctr: eoctr_t;
    signal tmt_sync: tmt_sync_t;
    
    signal clkmon: std_logic_vector(2 downto 0);
    
    signal leds: std_logic_vector(11 downto 0);
    
--    signal dt_data_sector, dt_data_sector_raw, super_sector, dt_data_sector_omtf : sector_t; --JSA--
    signal locked, d_locked, clk_stopped : std_logic;
--  signal test_en : std_logic;
    signal MAC_lsb : std_logic_vector(15 downto 0) := (others=>'0');
        
begin

-- Minipods enable
    reset_mp_rx_hr <= '1';    reset_mp_tx_hr <= '1';

-- Status LEDs
--    leds <= '1' & not led_q(1) & not led_q(0) & "111" & '1' & not (locked and onehz) & locked & '1' & not led_q(3) & not led_q(2);
------------------------------
    led1_yellow <= leds(1);  -- not led_q(3)
    led1_green  <= leds(0);  -- not led_q(2)
    led1_red    <= leds(10); -- not led_q(1)
    led1_orange <= clk_stopped; --leds(9);  -- not led_q(0)
    led2_yellow <= leds(4);  -- not (locked and onehz)
    led2_green  <= leds(3);  -- locked
    led2_red    <= locked;   -- DT sector locked
    led2_orange <= d_locked; -- DT sector d_locked

-- Clocks and control IO

    infra: entity work.mp7_infra
        generic map(
            MAC_ADDR => X"080030f30000", -- mac addr: 08:00:30:F3:00:00
            IP_ADDR =>  X"c0A80169"      -- fixed ip: 192.168.1.105
        )
        port map(
            gt_clkp => eth_clkp,
            gt_clkn => eth_clkn,
            gt_txp => eth_txp,
            gt_txn => eth_txn,
            gt_rxp => eth_rxp,
            gt_rxn => eth_rxn,
            leds => leds,
            clk_ipb => clk_ipb,
            rst_ipb => rst_ipb,
            clk40ish => clk40ish,
            refclk_out => eth_refclk,
            nuke => nuke,
            soft_rst => soft_rst,
            oc_flag => oc_flag,
            ec_flag => ec_flag,
            MAC_lsb => MAC_lsb,
            ipb_in_ctrl => ipb_out_ctrl,
            ipb_out_ctrl => ipb_in_ctrl,
            ipb_in_ttc => ipb_out_ttc,
            ipb_out_ttc => ipb_in_ttc,
            -- uROS...
            ipb_in_spiprogrammer  => ipb_out_spiprogrammer,
            ipb_out_spiprogrammer => ipb_in_spiprogrammer,
            ipb_in_readout => ipb_out_readout,
            ipb_out_readout => ipb_in_readout
            --...uROS
        );
        
        MAC_lsb <= x"02" & "11" & dipswitch(1 downto 0) & GA; -- range from 02:C0 to 02:FF

-- Control registers and board IO
        
    ctrl: entity work.mp7_ctrl
        port map(
            clk => clk_ipb,
            rst => rst_ipb,
            ipb_in => ipb_in_ctrl,
            ipb_out => ipb_out_ctrl,
            nuke => nuke,
            soft_rst => soft_rst,
            board_id => board_id,
            clk40_rst => clk40_rst,
            clk40_sel => clk40_sel,
            clk40_lock => clk40_lock,
            clk40_stop => clk40_stop
        );

-- TTC signal handling
    
    ttc: entity work.mp7_ttc
        port map(
            clk => clk_ipb,
            rst => rst_ipb,
            mmcm_rst => clk40_rst,
            sel => clk40_sel,
            lock => clk40_lock,
            stop => clk40_stop,
            ipb_in => ipb_in_ttc,
            ipb_out => ipb_out_ttc,
            clk40_in_p => clk40_in_p,
            clk40_in_n => clk40_in_n,
            clk40ish_in => clk40ish,
            clk20 => clk20,
            clk40 => clk40,
            clk80 => clk80,
            clk120 => clk120,
            clk200 => clk200,
            clk600 => clk600,
            rst40 => rst40,
            ttc_in_p => ttc_in_p,
            ttc_in_n => ttc_in_n,
            ttc_cmd => ttc_cmd,
            ttc_cmd_dist => ttc_cmd_dist,
            ttc_l1a => ttc_l1a,
            ttc_l1a_flag => ttc_l1a_flag,
            ttc_l1a_dist => ttc_l1a_dist,
            l1a_throttle => ttc_l1a_throttle,
            dist_lock => dist_lock,
            bunch_ctr => bunch_ctr_mp7,
            evt_ctr => evt_ctr,
            orb_ctr => orb_ctr,
            oc_flag => oc_flag,
            ec_flag => ec_flag,
            tmt_sync => tmt_sync,
            monclk => clkmon
        );

    dist_lock <= '1';
    
-- Readout

    readout: entity work.uros_readout
    port map(
    -- ports from mp7's readout
        clk => clk_ipb,
        rst => rst_ipb,
        ipb_in => ipb_in_readout,
        ipb_out => ipb_out_readout,
        ttc_clk => clk40,
        ttc_rst => rst40,
        ttc_cmd => ttc_cmd,
        l1a => ttc_l1a,
        bunch_ctr_mp7_in => bunch_ctr_mp7,
        orb_ctr => orb_ctr,            
        amc13_refclk => eth_refclk,
        -- uROS deser
        LVDS_in_p => LVDS_in_p(71 downto 0),    
        LVDS_in_n => LVDS_in_n(71 downto 0),

        SN_RXEN_HR   => SN_RXEN_HR(5 downto 0),
        SN_SD_HR     => SN_SD_HR(5 downto 0),
        SN_SQEN_HR   => SN_SQEN_HR(5 downto 0),
        SN_ENSD_HR   => SN_ENSD_HR(5 downto 0),
        
        clk40 => clk40,
        clk80 => clk40,
        clk120 => clk120,
        clk200 => clk200,
        clk600 => clk600,
        rst40 => rst40,

        AMC_P1_RX_P => AMC_P1_RX_P,
        AMC_P1_RX_N => AMC_P1_RX_N,
        AMC_P1_TX_N => AMC_P1_TX_N,
        AMC_P1_TX_P => AMC_P1_TX_P
    );

    ttc_l1a_throttle <= '1';
    SEL1_MGT_CLK_18 <= '0';    

--------------------------------------------------------------------------------
-- Spi Programmer controlled through IpBus
    tm7_ipbus_spiprogrammer: entity work.IpBus_SpiProgrammer
        generic map(
            FW_ID  => x"BEAF0001" -- TODO: tbv is there any other FW ID constant somewhere ?
        )
        port map(
            ------------------------------------------------------------------------
            -- IpBus
            clk     => clk_ipb,   -- clock: in the following assumes 125/4 MHz
            rst     => rst_ipb,
            ipb_in  => ipb_in_spiprogrammer,
            ipb_out => ipb_out_spiprogrammer,
            ------------------------------------------------------------------------
            -- SPI flash memory ports
            -- outSpiClk is output through STARTUPE2.USRCCLKO
            outSpiCsB   => outSpiCsB,
            outSpiMosi  => outSpiMosi,
            inSpiMiso   => inSpiMiso,
            outSpiWpB   => outSpiWpB,
            outSpiHoldB => outSpiHoldB
            ------------------------------------------------------------------------
            -- leds        => open  -- status Leds during programming phase (optional)
            ------------------------------------------------------------------------
        );


end rtl;
