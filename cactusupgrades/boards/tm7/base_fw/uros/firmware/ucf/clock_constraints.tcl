# Modified for TM7; original in boards/mp7/base_fw/common/firmware/ucf/
# Timing constraints for TM7

create_clock -period 10.000 -name clk_ref100 [get_ports clk_100]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks clk_ref100]

set_false_path -to [get_cells -hierarchical -filter {NAME =~ *data_sync_reg1}]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *reset_sync1*}]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *reset_sync2*}]


# Timing constraints for MP7

# TTC clock (40MHz) - external
create_clock -period 24.9 -name clk_40_ext [get_ports clk40_in_p]

# Ethernet RefClk (125MHz) - external
create_clock -period 8 -name eth_refclk [get_ports eth_clkp]

# Clock rate setting for refclks (kind of arbitrary, 125MHz here) - external
create_clock -name configurable_refclks -period 8.000 [get_ports {refclkn[*]}]

# Clock from Ethernet Transceiver (derived from Ethernet RefClk)
create_clock -period 16 -name eth_transceiver [get_pins infra/eth/phy/transceiver_inst/gtwizard_inst/gtwizard_v2_5_gbe_gth_i/gt0_gtwizard_v2_5_gbe_gth_i/gthe2_i/TXOUTCLK]

# The decoupled_clk is driven from a flip-flop to circumvent Xilinx rules for the ethernet sys clk.   Derived from eth_refclk.
create_clock -period 16 -name decoupled_clk [get_pins infra/eth/decoupled_clk_src_reg/Q]

# Fake 40MHz clock for tests without external clock source.  Derived from eth_refclk.
create_generated_clock -name clk_40_pseudo -source [get_pins infra/clocks/mmcm/CLKIN1] [get_pins infra/clocks/mmcm/CLKOUT2]

# IPBus Clock - Give it a sensible name - not "I".  Derived from eth_refclk.
create_generated_clock -name ipbus_clk -source [get_pins infra/clocks/mmcm/CLKIN1] [get_pins infra/clocks/mmcm/CLKOUT1]

# Block analysis of duplicate analysis of TTC clock path when clk_40_pseudo used.
set_case_analysis 1 [get_pins ttc/clocks/mmcm/CLKIN2]


# Clock relationship
    
# Clocks originating from eth_refclk
# Originally used single group for all clocks generated from eth_refclk.
set_clock_groups -asynch -group [get_clocks ipbus_clk]
set_clock_groups -asynch -group [get_clocks decoupled_clk]

# Clocks originating from ethernet transceiver txout clk
set_clock_groups -asynch -group [get_clocks -include_generated_clocks eth_transceiver]
set_clock_groups -asynch -group [get_clocks -include_generated_clocks clk_40_ext]
set_clock_groups -asynch -group [get_clocks -include_generated_clocks clk_40_pseudo]

# AMC13 transceiver clock
set_clock_groups -asynch -group [get_clocks -filter {NAME =~ readout/amc13/*/TXOUTCLK}]
