# Modified for TM7; original in boards/mp7/base_fw/common/firmware/ucf/pins_r1.tcl
# IO pin assignments for the TM7 board

# Clock IO

set_property PACKAGE_PIN L40 [get_ports {clk40_in_n}]
set_property IOSTANDARD LVDS [get_ports {clk40_in_n}]
set_property DIFF_TERM TRUE [get_ports {clk40_in_n}]
set_property PACKAGE_PIN AV38 [get_ports {ttc_in_n}]
set_property IOSTANDARD LVDS [get_ports {ttc_in_n}]
set_property DIFF_TERM TRUE [get_ports {ttc_in_n}]
set_property PACKAGE_PIN AB7 [get_ports eth_clkn]
set_property IOSTANDARD LVCMOS18 [get_ports {clk_100}]
set_property PACKAGE_PIN AJ32 [get_ports {clk_100}]

# MGT refclk
#set_property PACKAGE_PIN G9 [get_ports {refclkn[0]}]

# Front panel LEDs
set_property SLEW SLOW [get_ports {led*}]
set_property IOSTANDARD LVCMOS18 [get_ports {led*}]
set_property PACKAGE_PIN ah31 [get_ports {led1_yellow}]
set_property PACKAGE_PIN aj31 [get_ports {led1_green}]
set_property PACKAGE_PIN aj35 [get_ports {led1_red}]
set_property PACKAGE_PIN aj33 [get_ports {led1_orange}]
set_property PACKAGE_PIN an34 [get_ports {led2_yellow}]
set_property PACKAGE_PIN an33 [get_ports {led2_green}]
set_property PACKAGE_PIN ap35 [get_ports {led2_red}]
set_property PACKAGE_PIN ap37 [get_ports {led2_orange}]

# Minipod enable
set_property IOSTANDARD LVCMOS33 [get_ports {reset*}]
set_property PACKAGE_PIN bb34 [get_ports {reset_mp_rx_hr}]
set_property PACKAGE_PIN bb31 [get_ports {reset_mp_tx_hr}]

# Geographical address
set_property IOSTANDARD LVCMOS18 [get_ports {GA*}]
set_property PACKAGE_PIN AF36 [get_ports {GA[0]}]
set_property PACKAGE_PIN AF35 [get_ports {GA[1]}]
set_property PACKAGE_PIN AD36 [get_ports {GA[2]}]
set_property PACKAGE_PIN AD37 [get_ports {GA[3]}]

# Dipswitch
set_property IOSTANDARD LVCMOS18 [get_ports {dipswitch*}]
set_property PACKAGE_PIN V30 [get_ports {dipswitch[0]}]
set_property PACKAGE_PIN V31 [get_ports {dipswitch[1]}]
set_property PACKAGE_PIN T29 [get_ports {dipswitch[2]}]
set_property PACKAGE_PIN T30 [get_ports {dipswitch[3]}]
set_property PACKAGE_PIN W30 [get_ports {dipswitch[4]}]
set_property PACKAGE_PIN W31 [get_ports {dipswitch[5]}]
set_property PACKAGE_PIN V29 [get_ports {dipswitch[6]}]
set_property PACKAGE_PIN U29 [get_ports {dipswitch[7]}]

# LVDS inputs
set_property IOSTANDARD LVDS [get_ports {LVDS_in*}]
set_property DIFF_TERM TRUE [get_ports {LVDS_in*}]
set_property PACKAGE_PIN AB42 [get_ports {LVDS_in_n[0]}]  ;#SN1
set_property PACKAGE_PIN AD41 [get_ports {LVDS_in_n[1]}]
set_property PACKAGE_PIN AC39 [get_ports {LVDS_in_n[2]}]
set_property PACKAGE_PIN AA41 [get_ports {LVDS_in_n[3]}]
set_property PACKAGE_PIN AE38 [get_ports {LVDS_in_n[4]}]
set_property PACKAGE_PIN AG41 [get_ports {LVDS_in_n[5]}]
set_property PACKAGE_PIN AH38 [get_ports {LVDS_in_n[6]}]
set_property PACKAGE_PIN AJ41 [get_ports {LVDS_in_n[7]}]
set_property PACKAGE_PIN AL42 [get_ports {LVDS_in_n[8]}]
set_property PACKAGE_PIN AK42 [get_ports {LVDS_in_n[9]}]
set_property PACKAGE_PIN AH41 [get_ports {LVDS_in_n[10]}]
set_property PACKAGE_PIN AG42 [get_ports {LVDS_in_n[11]}]
set_property PACKAGE_PIN P40  [get_ports {LVDS_in_n[12]}] ;#SN2
set_property PACKAGE_PIN P42  [get_ports {LVDS_in_n[13]}]
set_property PACKAGE_PIN P38  [get_ports {LVDS_in_n[14]}]
set_property PACKAGE_PIN L41  [get_ports {LVDS_in_n[15]}]
set_property PACKAGE_PIN R34  [get_ports {LVDS_in_n[16]}]
set_property PACKAGE_PIN R37  [get_ports {LVDS_in_n[17]}]
set_property PACKAGE_PIN U38  [get_ports {LVDS_in_n[18]}]
set_property PACKAGE_PIN W37  [get_ports {LVDS_in_n[19]}]
set_property PACKAGE_PIN AA39 [get_ports {LVDS_in_n[20]}]
set_property PACKAGE_PIN W42  [get_ports {LVDS_in_n[21]}]
set_property PACKAGE_PIN V40  [get_ports {LVDS_in_n[22]}]
set_property PACKAGE_PIN T42  [get_ports {LVDS_in_n[23]}]
set_property PACKAGE_PIN D40  [get_ports {LVDS_in_n[24]}] ;#SN3
set_property PACKAGE_PIN E42  [get_ports {LVDS_in_n[25]}]
set_property PACKAGE_PIN D36  [get_ports {LVDS_in_n[26]}]
set_property PACKAGE_PIN C39  [get_ports {LVDS_in_n[27]}]
set_property PACKAGE_PIN F37  [get_ports {LVDS_in_n[28]}]
set_property PACKAGE_PIN G38  [get_ports {LVDS_in_n[29]}]
set_property PACKAGE_PIN J35  [get_ports {LVDS_in_n[30]}]
set_property PACKAGE_PIN L37  [get_ports {LVDS_in_n[31]}]
set_property PACKAGE_PIN L42  [get_ports {LVDS_in_n[32]}] 
set_property PACKAGE_PIN J41  [get_ports {LVDS_in_n[33]}]
set_property PACKAGE_PIN H41  [get_ports {LVDS_in_n[34]}]
set_property PACKAGE_PIN G42  [get_ports {LVDS_in_n[35]}]
set_property PACKAGE_PIN D32  [get_ports {LVDS_in_n[36]}] ;#SN4
set_property PACKAGE_PIN A34  [get_ports {LVDS_in_n[37]}]
set_property PACKAGE_PIN G29  [get_ports {LVDS_in_n[38]}]
set_property PACKAGE_PIN G27  [get_ports {LVDS_in_n[39]}]
set_property PACKAGE_PIN K30  [get_ports {LVDS_in_n[40]}] 
set_property PACKAGE_PIN F31  [get_ports {LVDS_in_n[41]}]
set_property PACKAGE_PIN G33  [get_ports {LVDS_in_n[42]}]
set_property PACKAGE_PIN D33  [get_ports {LVDS_in_n[43]}]
set_property PACKAGE_PIN C41  [get_ports {LVDS_in_n[44]}]
set_property PACKAGE_PIN B42  [get_ports {LVDS_in_n[45]}]
set_property PACKAGE_PIN A39  [get_ports {LVDS_in_n[46]}]
set_property PACKAGE_PIN A37  [get_ports {LVDS_in_n[47]}]
set_property PACKAGE_PIN B24  [get_ports {LVDS_in_n[48]}] ;#SN5
set_property PACKAGE_PIN B27  [get_ports {LVDS_in_n[49]}]
set_property PACKAGE_PIN B23  [get_ports {LVDS_in_n[50]}]
set_property PACKAGE_PIN G22  [get_ports {LVDS_in_n[51]}]
set_property PACKAGE_PIN E24  [get_ports {LVDS_in_n[52]}]
set_property PACKAGE_PIN K25  [get_ports {LVDS_in_n[53]}]
set_property PACKAGE_PIN E25  [get_ports {LVDS_in_n[54]}]
set_property PACKAGE_PIN D26  [get_ports {LVDS_in_n[55]}]
set_property PACKAGE_PIN B31  [get_ports {LVDS_in_n[56]}]
set_property PACKAGE_PIN B29  [get_ports {LVDS_in_n[57]}]
set_property PACKAGE_PIN A14  [get_ports {LVDS_in_n[60]}]        ;# sch: LVDS_58_N
set_property PACKAGE_PIN J15  [get_ports {LVDS_in_n[61]}]        ;# sch: LVDS_59_N
set_property PACKAGE_PIN B16  [get_ports {LVDS_in_n[62]}]        ;# sch: LVDS_60_N
set_property PACKAGE_PIN A15  [get_ports {LVDS_in_n[63]}]        ;# sch: LVDS_61_N
set_property PACKAGE_PIN D17  [get_ports {LVDS_in_n[64]}]        ;# sch: LVDS_62_N
set_property PACKAGE_PIN A19  [get_ports {LVDS_in_n[65]}]        ;# sch: LVDS_63_N
set_property PACKAGE_PIN C20  [get_ports {LVDS_in_n[66]}]        ;# sch: LVDS_64_N
set_property PACKAGE_PIN A21  [get_ports {LVDS_in_n[67]}]        ;# sch: LVDS_65_N
set_property PACKAGE_PIN G16  [get_ports {LVDS_in_n[68]}]        ;# sch: LVDS_66_N
set_property PACKAGE_PIN E15  [get_ports {LVDS_in_n[69]}]        ;# sch: LVDS_67_N
set_property PACKAGE_PIN D15  [get_ports {LVDS_in_n[70]}]        ;# sch: LVDS_68_N
set_property PACKAGE_PIN F14  [get_ports {LVDS_in_n[71]}]        ;# sch: LVDS_69_N
set_property PACKAGE_PIN C21  [get_ports {LVDS_in_n[58]}]        ;# sch: LVDS_70_N
set_property PACKAGE_PIN H20  [get_ports {LVDS_in_n[59]}]        ;# sch: LVDS_71_N

# Avago control
set_property IOSTANDARD LVCMOS33 [get_ports {SN_*}]
set_property PACKAGE_PIN AV30 [get_ports {SN_RXEN_HR[0]}]
set_property PACKAGE_PIN AW30 [get_ports {SN_SD_HR[0]}]
set_property PACKAGE_PIN AW31 [get_ports {SN_SQEN_HR[0]}]
set_property PACKAGE_PIN BA30 [get_ports {SN_ENSD_HR[0]}]
set_property PACKAGE_PIN AV33 [get_ports {SN_RXEN_HR[1]}]
set_property PACKAGE_PIN BA32 [get_ports {SN_SD_HR[1]}]
set_property PACKAGE_PIN AU33 [get_ports {SN_SQEN_HR[1]}]
set_property PACKAGE_PIN AY32 [get_ports {SN_ENSD_HR[1]}]
set_property PACKAGE_PIN BA31 [get_ports {SN_RXEN_HR[2]}]
set_property PACKAGE_PIN AY30 [get_ports {SN_SD_HR[2]}]
set_property PACKAGE_PIN AW32 [get_ports {SN_SQEN_HR[2]}]
set_property PACKAGE_PIN AV31 [get_ports {SN_ENSD_HR[2]}]
set_property PACKAGE_PIN BA34 [get_ports {SN_RXEN_HR[3]}]
set_property PACKAGE_PIN AY33 [get_ports {SN_SD_HR[3]}]
set_property PACKAGE_PIN AY34 [get_ports {SN_SQEN_HR[3]}]
set_property PACKAGE_PIN AU34 [get_ports {SN_ENSD_HR[3]}]
set_property PACKAGE_PIN AW35 [get_ports {SN_RXEN_HR[4]}]
set_property PACKAGE_PIN BA35 [get_ports {SN_SD_HR[4]}]
set_property PACKAGE_PIN BA36 [get_ports {SN_SQEN_HR[4]}]
set_property PACKAGE_PIN AV34 [get_ports {SN_ENSD_HR[4]}]
set_property PACKAGE_PIN AW36 [get_ports {SN_RXEN_HR[5]}]
set_property PACKAGE_PIN AY35 [get_ports {SN_SD_HR[5]}]
set_property PACKAGE_PIN AV36 [get_ports {SN_SQEN_HR[5]}]
set_property PACKAGE_PIN AV35 [get_ports {SN_ENSD_HR[5]}]

# Readout pins
set_property PACKAGE_PIN AH28 [get_ports SEL1_MGT_CLK_18]
set_property IOSTANDARD LVCMOS18 [get_ports SEL1_MGT_CLK_18]
set_property PACKAGE_PIN AC2 [get_ports AMC_P1_TX_P]
set_property PACKAGE_PIN AB4 [get_ports AMC_P1_RX_P]
set_property PACKAGE_PIN AC1 [get_ports AMC_P1_TX_N]
set_property PACKAGE_PIN AB3 [get_ports AMC_P1_RX_N]