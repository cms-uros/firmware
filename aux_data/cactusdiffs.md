This file explains the relationship of the /cactusupgrades folder to the cactusupgrades folder of the mp7 project at https://gitlab.cern.ch/cms-cactus/firmware/mp7 and the ipbus sources at https://github.com/ipbus/firmware

This version of the uROS fw is based on the 2.4.0 tag of the mp7 project.

## boards/tm7/

this folder contains board-level code both common to all tm7 firmwares (./common) and also specific to uROS (./uros). Many of the files in this folder have been modified from a mp7 file. This is indicated in the first line of each file, and should be taken into account when backporting changes from the mp7 repository.

## boards/mp7/

every file in this folder is directly copied from its original mp7 file.

## components/

this folder contains the code for the different firmware blocks used in the design. Some of the blocks are specific to uros, while some others come from ipbus and some others were originally developed for mp7. With the tm7/ab7-specific modules, no confussion is possible because there is no need to backport any changes. The ipbus and mp7 modules should be identical to the ones either at the mp7 repo tag on which this fw is based, or the ipbus github tag in which the mp7 tag is based. This is not, however true for all modules, and here comes a list of the changes to take into account:

- mp7_ttc/ we have modified the clocks so there are changes to 2 files here (mp7_ttc.vhd, ttc_clocks.vhd) and also have changed the BGOs (mp7_ttc_decl.vhd).


